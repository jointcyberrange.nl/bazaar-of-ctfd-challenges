# Status: OK

# veracrypt

# Description
Je kunt bestanden al helemaal goed verbergen door VeraCrypt te installeren. Windows computers vinden het erg lastig om linux ext4 geformatteerd geheugen uit te moeten lezen.

Vind de verborgen vlag in de bijgeleverde VeraCrypt file container.

Wachtwoord van container = linux_is_awesome

Flag = JCR(...)