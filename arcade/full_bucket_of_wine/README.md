# Status: OK

# Full_bucket_of_wine

# Description
Download de bijgeleverde Full_Bucket_Of_Wine binary in een Linux omgeving. Open een terminal en ga naar de map waar je de binary hebt opgeslagen. Activeer het bestand door het commando " ./Full_Bucket_Of_Wine " uit te voeren en volg de instructies. Als het bestand niet meteen werkt, misschien moet je het nog uitvoeringsrechten geven met het commando " chmod +x Full_Bucket_Of_Wine ".

Kun jij de vlag achterhalen?

Flag format = JCR(een nummer)