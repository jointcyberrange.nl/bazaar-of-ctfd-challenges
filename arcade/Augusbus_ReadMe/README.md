# Status: OK

# 8. Augusbus

# Description
Net zoals wij met de bus van A naar B kunnen reizen, kunnen wij binnen computerland ook een bus gebruiken om data te versturen tussen verschillende componenten in een computersysteem: https://en.wikipedia.org/wiki/Bus_(computing)

Helaas zijn deze computer bussen niet altijd even veilig.

Zoals Pieter Burghouwt vertelt in aflevering 1 van de TechnicalSecurity101 podcast \[1\], is Wikipedia trouwens een prima plek om te beginnen als je niet weet wat bepaalde termen uit het werkveld betekenen. 

Voor de puzzels van deze maand dien je Wireshark te installeren op je Linux omgeving en zul je gaan oefenen met het scripten in de programmeertaal Python. Voor beide zijn online de nodige tutorials te vinden.

Zijn we er klaar voor!

JCR(Yes!)

1. https://anchor.fm/ts101