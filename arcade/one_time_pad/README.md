# Status: OK

# One-time pad

# Description
Upzip het bijgeleverde xor.zip bestand en bestudeer het stukje code crypto_easy.py en het output.txt bestand.

One-time pad, de sterkste manier om informatie te versleutelen! Als de sleutels ieder maar 1x gebruikt zouden worden..

Kun jij de ..redacted.. keys achter halen en het patroon ontdekken waarmee de messages zijn versleuteld?  

Flag format = JCR(...flag...)