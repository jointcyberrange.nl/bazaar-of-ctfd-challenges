# Status: OK

# 11. Pwnovember

# Description
PWNing, het compleet overnemen (OWNen) van een applicatie!

https://www.youtube.com/playlist?list=PLchBW5mYosh_F38onTyuhMTt2WGfY-yr7

Flag = JCR(Gaaf!)

In voorbereiding op de nationale scholencompetitie CTF is dit de laatste maand met puzzels. Succes!