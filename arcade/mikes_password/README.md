# Status: OK

# Mike's password

# Description
Mike encrypted his password on piece of paper:

EpztkGgtsgbha

Now he forgot how he encrypted it. He only knows that the key is crypto. Can you help him to find the password back?