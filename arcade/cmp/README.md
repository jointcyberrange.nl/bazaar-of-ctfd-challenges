# Status: OK

# cmp

## Description
Naast "diff" en "md5sum" kun je binnen linux met het "cmp" commando zelfs op byte niveau bestanden met elkaar vergelijken.

Zoek de verborgen vlag door te kijken naar de hele kleine verschillen tussen matrix_meme1 en matrix_meme2.

De vlag zal beginnen met JCR(

## Topics
- Byte-Niveau
- Command Line tools
