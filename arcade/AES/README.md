# Status: OK

# AES

# Description
AES, de Advanced Encryption Standard, die in bijna alle applicaties nu wordt gebruikt!

Bestudeer de versleutelde berichten in het bijgeleverde messages.txt bestand.

Aanvaller Eve heeft het vermoede dat deze berichten tussen Alice en Bob versleuteld zijn met AES en gokt erop dat er onhandige keuzes zijn gemaakt in de implementatie door het gebruik van een onveilige modus van AES en door het gebruik van een verouderd, maar nog veel gebruikt Hashing algoritme. 

Eve mag 1 van de 4 berichten vervangen met een nieuw versleuteld bericht. 

Flag format = JCR(het nieuwe versleutelde bericht)

Deze puzzel is best logisch als je er een beetje nadenkt, waarbij 'flag' de sleutel is tot succes! 