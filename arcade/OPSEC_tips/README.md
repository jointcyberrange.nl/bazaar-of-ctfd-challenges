# Status: OK

# OPSEC Tips

# Description
Of je nu online games speelt, filmpjes kijkt, blogs leest of handeld in crypto, een goede OPSEC (operational security) doet wonderen voor je veiligheid.

1. Als het goed is gebruik je in deze publieke omgeving niet je echte naam,
2. log je in met een junk mailadres en je niet prive mailadres,
3. gebruik je een VPN connectie om je IP-adres te verbergen,
4. en maak je gebruik van een virtuele machine en snapshots bij het oplossen van de puzzels.

Check, check, dubbel check?

Flag = JCR(Yes!)