# Status: OK

# 7. Julinux

# Description
Zoals Peter Rong vertelt in aflevering 4 van de TechnicalSecurity101 podcast \[1\], zijn vaardigheden in het gebruik van Linux essentieel voor een cybersecurity specialist.  

De puzzels voor deze maand zijn dan ook het makkelijkst om op te lossen in een Linux virtuele machine:

https://www.howtogeek.com/796988/how-to-install-linux-in-virtualbox/

https://linuxjourney.com/ OR https://linuxsurvival.com/



Check?

Flag = JCR(Yes!)

1. https://anchor.fm/ts101/