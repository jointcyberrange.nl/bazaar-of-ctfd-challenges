# Status: OK

# Sips_Of_Wine

# Description
Open het bijgeleverde 64_sips_of_wine bestand in een linux omgeving en probeer het wachtwoord te raden.
Dit wachtwoord is ook meteen de flag van deze puzzel.

Tools: Ghidra en Cyberchef

Flag = JCR(wachtwoord van 13 symbolen)