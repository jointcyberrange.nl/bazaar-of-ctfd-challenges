# Status: OK

# 10. Cryptober

# Description
Cryptography. Het versleutelen en ontsleutelen van informatie (en nee, niet onzin als Blockchain \[1\]). Zonder crypto zouden wij niet veilig op het Internet kunnen en was het Internet een stuk minder te vertrouwen.

Dat overheden versleutelde informatie maar in de weg vinden zitten is begrijpelijk, want fraude, terrorisme en kinder porno, maar het weghalen of verzwakken van versleutelde informatie brengt iedereen in gevaar.

Het versleutelen van informatie is zo sterk of zo zwak als waar jij de sleutels hebt verstopt. Denk maar eens na. Zijn de Whatsapp berichten tussen jou en je vrienden en familie wel echt End-to-End versleuteld, als Facebook / Meta de sleutels heeft?

Voor de puzzels van deze maand ga je weer verder oefenen met het programmeren in Python. Hebben wij er zin in!

Flag = JCR(Yes!)

1. https://www.schneier.com/blog/archives/2022/06/on-the-dangers-of-cryptocurrencies-and-the-uselessness-of-blockchain.html

