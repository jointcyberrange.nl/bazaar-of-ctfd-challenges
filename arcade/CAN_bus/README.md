# Status: OK

# CAN bus

# Description
Het CAN bus protocol wordt in veel auto's gebruikt om electronische processen aan te sturen, zoals gasgeven en remmen. Net als Modbus is CAN bus niet een veilige manier van communiceren. Ook hier hebben wij wat code voor om dit te simuleren, met dank aan: https://cjhackerz.net/posts/can-bus_protocol_pentesting/

Analyseer het bijgeleverde canbus-traffic-capture.pcap bestand. Hoe snel hebben wij ons kleine autootje (wiel radius m 0.01832) laten rijden door CAN bus berichten te sturen naar de motor?

Flag = JCR(km/h afgerond op heel getal)

Extra: Overzichtelijke presentatie over hoe auto's te hacken zijn, met voorbeelden van 2010 tot aan 2022: https://media.ccc.de/v/mch2022-252-a-brief-history-of-automotive-insecurities
