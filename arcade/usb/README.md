# Status: OK

# usb

# Description
Een toetsenbord is eigenlijk ook gewoon een USB apparaat. Deze blogpost vertelt je hoe je wireshark kunt gebruiken om mee te kijken met het USB verkeer van en naar je computer: https://blog.davehylands.com/capturing-usb-serial-using-wireshark/
Bijvoorbeeld op het moment dat iemand een gebruikersnaam en/of wachtwoord intoetst.

Analyseer het bijgeleverde usb_keyboard.pcapng bestand. Kun jij de ingetoetste vlag achterhalen?
Let op, deze vlag heeft qua syntax een net iets andere vorm dan de andere puzzels. 

Flag = jcr\[...\]

Extra: Vermakelijke presentatie over hoe barcode scanners ook USB apparaten zijn: https://media.ccc.de/v/mch2022-254-everything-is-an-input-device-fun-with-barcodes-