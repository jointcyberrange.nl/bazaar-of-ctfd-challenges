# Status: OK

# md5sum

## Description
Vanuit een linux terminal kun je gemakkelijk bekijken of twee bestanden dezelfde inhoud hebben, met bijvoorbeeld het "diff" commando of door de md5 hash waardes van de bestanden te vergelijken met "md5sum".

Maak binnen je linux omgeving een .txt bestand aan met daarin de tekst "hello world" en controleer of md5 hash daarvan de volgende is:
6f5902ac237024bdd0c176cb93063dc4

Verander nu het bestand van naam en zie dat hierdoor de md5 hash waarde niet veranderd.

Wat de md5 hash waarde van het bestand als je nu de tekst aanpast naar "hello bye"?

Flag = JCR(hash)

## Topics
- Hashing
- MD5
- Command line tools

