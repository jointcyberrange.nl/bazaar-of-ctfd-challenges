# Status: OK

# modbus

# Description
Het Modbus protocol wordt in veel fabrieken gebruikt om via PLCs (programmable logic controllers) chemische processen aan te sturen. Modbus is alleen niet een veilige manier van communiceren.  Uitdagingen in de beveiliging van OT (operational technology) en ICS (industrial control system) omgevingen zijn op een leuke en veilige manier te simuleren met: https://github.com/djformby/GRFICS 

Analyseer het bijgeleverde modbus.pcapng bestand. Met welke register-waarde wijziging hebben wij in onze simulatie, via het modbus protocol het chemische proces hebben crashen? Niet te moeilijk denken, er zit een logische gedachte achter.

Flag = JCR(register-waarde)

Extra: Boeiende presentatie over kwetsbaarheden in ICS:
https://media.ccc.de/v/mch2022-294-ics-stands-for-insecure-control-systems