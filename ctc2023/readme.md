# Challenge the Cyber 2023

This folder is based off the Dutch national competition in 2023.
<https://www.challengethecyber.nl> and <https://ctc2023.jointcyberrange.nl>

Install through e.g. `ls -1 -d ctc2023/* | xargs -L 1 ctf challenge install`

Status: partially ready
