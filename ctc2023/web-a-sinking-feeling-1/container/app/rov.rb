# frozen_string_literal: true

require 'sinatra/base'
require 'sinatra/cookies'
require 'socket'
require 'timeout'

class Rov < Sinatra::Base
  helpers Sinatra::Cookies

  set :static, true
  set :static_cache_control, [:public, {:max_age => 3600}]

  def failed_login
    redirect '/#failed_login', 302
  end

  get '/' do
    erb :login
  end

  post '/login' do
    failed_login unless params.key?(:username)
    failed_login unless params.key?(:password)
    failed_login unless params[:username] == 'admin'
    failed_login unless params[:password] == 'admin'
    
    cookies[:admin] = true
    redirect '/control', 302
  end

  get '/control' do
    failed_login unless cookies.key?(:admin)
    failed_login unless cookies[:admin]

    erb :control
  end

  get '/live-data' do
    failed_login unless cookies.key?(:admin)
    failed_login unless cookies[:admin]

    output = ''
    c = params[:ipaddress].split(':')
    raise "Hosts other than '127.0.0.1' not supported." unless c[0] == '127.0.0.1'

    Socket.tcp(c[0], c[1].to_i, connect_timeout: 1) do |sock|
      sock.puts params[:query]
      Timeout.timeout(3) do
        while line = sock.read(1)
          output += line
        end
      end
      sock.close
    end
    output
  rescue Errno::ECONNRESET
    output
  rescue Exception => e
    "Output: #{output}\nError: #{e.to_s}"
  end
end
