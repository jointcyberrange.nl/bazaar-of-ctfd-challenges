# Status: FLAG-MISSING
source: https://gitlab.com/jointcyberrange.nl/challenges-2023/-/tree/main/challenges/ctc2023-misc-quackery?ref_type=heads



Synopsis

REPL of a concatenative programming language (quackery).
Escape the REPL to execute arbitrary Python code.

Walkthrough

Input containing "python" is detected and the program is terminated.
The following input can be used to bypass the check, effectively by evaluating a concatenated string:

$ "print(os.popen('id').read())" [ $ "pyt" $ "hon" join ] quackery


The participant should execute a binary on the system to prove they have code exec (getflag).