
def monkey_input(x):
	ret = original_input(x)
	if "python" in ret:
		print("[!!!!] YOU SCARED AWAY THE DUCK. DUCKS DONT LIKE SNAKES")
		print("[!!!!] We've patched our quackery against you. Snakes be gone!")
		sys.exit(1)

	return ret

__builtins__.input = monkey_input

from quackery import quackery, QuackeryError

print("""
Welcome to the Quackery, a safe haven for all the cutest quacks!

Commands: 
	- quack 	: Quack like a duck
	- impersonate	: Impersonate fish
	- collect	: Collect some breadcrumbs
""")


quackscript = """
[ say "Quack quack" ] 	is quack
[ say "Blub blub" ] 	is impersonate
[ 1 ]			is collect

shell
"""

try:
	quackery(quackscript)
except QuackeryError as diagnostics:
	print('\nQuackery crashed.\n')
	print(diagnostics)
	print()
