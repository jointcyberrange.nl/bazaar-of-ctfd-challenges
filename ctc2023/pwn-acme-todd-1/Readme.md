# Status: OK
# ACME TODD

## Synopsis
Participants are given access to a network service which is supposedly an access terminal to a system made by ACME called Tactical Oceanice Disruption Ducky - shortened to TODD.

The are tasked with finding as much information as they can about TODD, which boils down to two tasks and two flags.

For the first task, they have to exploit a stack buffer overflow in a binary running on an emulated RP2040 device, which is very similar to the hardware ducky they've been provided. After successfully exploiting the RP2040 binary, they're dropped into a second challenge for the second flag.

All the binaries that the participants are expected to exploit are provided to them to simplify the task.

## Walktrhough

Source: https://gitlab.com/jointcyberrange.nl/challenges-2023/-/tree/main/challenges/ctc2023-pwn-acme-todd?ref_type=heads