// Build with: gcc -m32 -no-pie -fno-stack-protector -static -O2 ./chall.c -o debug_terminal.elf -lseccomp

#if defined(_FORTIFY_SOURCE)
#undef _FORTIFY_SOURCE
#define _FORTIFY_SOURCE 0
#endif

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <stdint.h>
#include <ctype.h>
#include <errno.h>
#include <unistd.h>
#include <sys/utsname.h>
#include <seccomp.h>


const uint32_t KEY_SIZE = 32;
const uint32_t KEY_SIZE_HEX = KEY_SIZE * 2;

static FILE *flag_file = NULL;
static char REVISION[1024];
static char BUILD_DATE[1024];
static char AUTH_KEY[1024];

bool parse_params(FILE *datafile)
{
    char buf[4096];
    memset(buf, 0, sizeof(buf));
    size_t readc = fread(buf, 1, sizeof(buf), datafile);
    if (readc == sizeof(buf))
        return false;

    buf[readc] = (char)0;

    memset(REVISION, 0, sizeof(REVISION));
    memset(BUILD_DATE, 0, sizeof(BUILD_DATE));
    memset(AUTH_KEY, 0, sizeof(AUTH_KEY));

    char *start = buf;
    char *end = &buf[readc];
    char *eol = strstr(start, "\n");

    while (start < end && eol)
    {
        *eol = (char)0;
        char *line = start;

        if (strstr(line, "revision="))
        {
            strcpy(REVISION, &line[strlen("revision=")]);
        }
        else if (strstr(line, "date="))
        {
            strcpy(BUILD_DATE, &line[strlen("date=")]);
        }
        else if (strstr(line, "key="))
        {
            strcpy(AUTH_KEY, &line[strlen("key=")]);
        }
        else if (strstr(line, "flag="))
        {
            // Do nothing
        }
        else
        {
            // Unknown, do nothing
        }

        start = eol + 1;
        eol = strstr(start, "\n");
    }

    return REVISION[0] != (char)0 && BUILD_DATE[0] != (char)0 && AUTH_KEY[0] != (char)0;
}

void __attribute__((constructor)) initializer()
{
    flag_file = fopen("/flag.txt", "r");
    if (flag_file == NULL)
    {
        perror("Failed to open flag file!\n");
        exit(1);
    }
    if (!parse_params(flag_file)) {
        perror("Failed to parse flag file!\n");
        exit(2);
    }

    // Kill process by default
    scmp_filter_ctx ctx = seccomp_init(SCMP_ACT_KILL_PROCESS);
    // Allow exit
    seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(exit_group), 0);

    // Allow libc heap stuff + mmap
    seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(brk), 0);
    seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap), 0);
    seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(mmap2), 0);
    seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(munmap), 0);

    // Allow file stuff, strict
    seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(read), 1, SCMP_A0_32(SCMP_CMP_EQ, 0));
    seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(write), 1, SCMP_A0_32(SCMP_CMP_LE, 2));
    seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(close), 0);

    // Misc allowed
    seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(uname), 0);
    seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(getcwd), 0);
    seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(gettimeofday), 0);

    if (seccomp_load(ctx)) {
        perror("Failed to load seccomp profile!\n");
        exit(3);
    }

    seccomp_release(ctx);
}

uint8_t nibble_from_hex_digit(char c)
{
    if ('0' <= c && c <= '9')
        return (uint8_t)(c - '0');
    if ('A' <= c && c <= 'F')
        return (uint8_t)((c - 'A') + 10);
    return (uint8_t)((c - 'a') + 10);
}

bool is_hex_digit(char c) {
    if ('0' <= c && c <= '9')
        return true;
    if ('A' <= c && c <= 'F')
        return true;
    return ('a' <= c && c <= 'f');
}

bool check_raw_key(char * key) {
    return strncmp(key, AUTH_KEY, sizeof(AUTH_KEY)) == 0;
}

bool check_hex_key(char * key) {
    char keybuf[KEY_SIZE];
    for (size_t i = 0; i < KEY_SIZE; ++i)
    {
        size_t hexidx = i << 1;
        uint8_t val = nibble_from_hex_digit(key[hexidx]) << 4;
        val |= nibble_from_hex_digit(key[hexidx+1]);
        keybuf[i] = (char)val;
    }
    return check_raw_key(keybuf);
}

bool check_key(char format) {
    char buf[KEY_SIZE];

    if (format == 'h') {
        // BUG: oops, used the wrong size here!
        fread(buf, 1, KEY_SIZE_HEX, stdin);
        for (size_t i = 0; i < KEY_SIZE_HEX; ++i) {
            if (!is_hex_digit(buf[i])) {
                printf("[AUTH] Invalid hex key!\n");
                return false;
            }
        }
        return check_hex_key(buf);
    }

    if (format == 'r') {
        fread(buf, 1, KEY_SIZE, stdin);
        return check_raw_key(buf);
    }

    printf("[AUTH] Unknown format!\n");
    return false;
}

int main(int argc, char *argv[])
{
    bool authenticated = false;
    struct utsname uname_data;

    setvbuf(stdin, NULL, _IONBF, 0);
    setvbuf(stdout, NULL, _IONBF, 0);
    setvbuf(stderr, NULL, _IONBF, 0);

    printf("[MAIN SYSTEM] Welcome to TODD main system debug terminal\n");
    printf("[MAIN SYSTEM] Build revision: %s\n", REVISION);
    printf("[MAIN SYSTEM] Build date: %s\n", BUILD_DATE);
    if(uname(&uname_data) != -1)
    {
        printf("[MAIN SYSTEM] Running on %s %s %s\n", uname_data.sysname, uname_data.machine, uname_data.nodename);
    }

    while (!authenticated) {
        char input[16];
        char choice;
        printf("[AUTH] A key is required to continue.\n");
        printf("[AUTH] Please choose the key format (h = hex | r = raw): ");

        if (fgets(input, sizeof(input), stdin) == NULL)
            break;

        sscanf(input, " %c", &choice);

        authenticated = check_key((char)tolower(choice));

        if (!authenticated) {
            printf("[AUTH] Invalid key!\n");
        }

    }

    if (authenticated) {
        printf("[AUTH] Authentication successful!\n");
        printf("[MAIN SYSTEM] This is an unlicensed build of TODD. Terminal features are disabled.\n");
        printf("[MAIN SYSTEM] Flag ACME sales down with your contact information and build: %s\n", REVISION);
    }

    printf("[MAIN SYSTEM] Thank you for using TODD debug terminal. Goodbye!\n");
    return 0;
}