#include <stdio.h>
#include <pico/stdlib.h>
#include <hardware/watchdog.h>
#include <hardware/gpio.h>
#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>

const uint32_t GPIO_LO = 0;
const uint32_t GPIO_HI = 1;
const uint32_t FLASH_KEY_OFFSET = 0x10000;
const uint32_t FLASH_START_ADDR = 0x10000000;
const uint32_t FLASH_KEY_ADDR = FLASH_START_ADDR + FLASH_KEY_OFFSET;

static const char *SYSTEM_NAME_BANNER =
    "    ___   ________  _________   __________  ____  ____ \n"
    "   /   | / ____/  |/  / ____/  /_  __/ __ \\/ __ \\/ __ \\\n"
    "  / /| |/ /   / /|_/ / __/      / / / / / / / / / / / /\n"
    " / ___ / /___/ /  / / /___     / / / /_/ / /_/ / /_/ / \n"
    "/_/  |_\\____/_/  /_/_____/    /_/  \\____/_____/_____/  \n"
    "                                                       \n"
    "                                                   (tm)\n";

static const char *SYSTEM_DESC =
    "[INITIALIZATION] TODD SYSTEMS STARTING UP...\n"
    "[INITIALIZATION] ENGAGING FLAPPY SYSTEMS...\n"
    "[INITIALIZATION] FLAPPY SYSTEMS ENGAGED...\n"
    "[INITIALIZATION] ARMING OCEANINC DISRUPTION SYSTEMS...\n"
    "[INITIALIZATION] OD SYSTEMS FAILED TO ARM!\n"
    "[INITIALIZATION] THIS COPY OF TODD (tm) SYSTEMS IS UNLICENSED.\n"
    "\tPLEASE CONTACT ACME SALES FOR A LICENSE\n"
    "\tAND SUPPLY THE LICENSE FILE AT THE DEBUG TERMINAL.\n"
    "[INITIALIZATION] UNLICENSED TODD SESSION TIME IS LIMITED TO 5 MINUTES!";

const uint32_t ACME_TERMINAL_ENABLE_PIN = 4;
const uint32_t ACME_TERMINAL_READY_PIN = 5;
const uint32_t ACME_SHUTDOWN_PIN = 6;
const uint32_t UNLICENSED_USE_TIME_MS = 5 * 60 * 1000;

static volatile bool done = false;
static volatile bool timed_out = false;

void software_reset()
{
    watchdog_enable(1, 1);
    while (1)
    {
    };
}

void login_banner()
{
    puts(SYSTEM_NAME_BANNER);
    puts(SYSTEM_DESC);
}

void goodbye()
{
    if (timed_out)
    {
        printf("UNLICENSED USE TIME IS OVER. ");
    }
    printf("GOOD BYE!\n");
    sleep_ms(100);
    gpio_put(ACME_SHUTDOWN_PIN, GPIO_HI);
    do
    {
        sleep_ms(1000);
    } while (true);
}

void switch_debug_terminal()
{
    puts("[DEBUG TERMINAL] ENABLING TODD MAIN CONTROL");
    const size_t MAX_ATTEMPT_COUNT = 10;
    const size_t MS_WAIT_PER_ATTEMPT = 500;

    size_t attempt_count = 0;
    size_t value = GPIO_LO;
    while ((value = gpio_get(ACME_TERMINAL_READY_PIN)) != GPIO_HI)
    {
        if (attempt_count >= MAX_ATTEMPT_COUNT)
        {
            break;
        }
        sleep_ms(MS_WAIT_PER_ATTEMPT);
        attempt_count += 1;
    }

    if (value == GPIO_HI)
    {
        puts("[DEBUG TERMINAL] TODD MAIN CONTROL ENABLED");
        sleep_ms(100);
        gpio_put(ACME_TERMINAL_ENABLE_PIN, GPIO_HI);
        sleep_ms(100);
        software_reset();
    }
    else
    {
        puts("[DEBUG TERMINAL] MAIN CONTROL TERMINAL UNRESPONSIVE");
        puts("[DEBUG TERMINAL] ENABLING TODD MAIN CONTROL FAILED");
    }
}

int64_t unlicensed_timeout(alarm_id_t id, void *user_data)
{
    if (user_data == (void *)0xdeadbeef)
    {
        done = true;
        timed_out = true;
    }
    return 0;
}

void initialize_gpios()
{
    gpio_init(ACME_TERMINAL_READY_PIN);
    gpio_init(ACME_TERMINAL_ENABLE_PIN);
    gpio_init(ACME_SHUTDOWN_PIN);

    gpio_set_dir(ACME_TERMINAL_READY_PIN, GPIO_IN);
    gpio_set_dir(ACME_TERMINAL_ENABLE_PIN, GPIO_OUT);
    gpio_set_dir(ACME_SHUTDOWN_PIN, GPIO_OUT);

    gpio_put(ACME_TERMINAL_ENABLE_PIN, GPIO_LO);
    gpio_put(ACME_SHUTDOWN_PIN, GPIO_LO);
}

void initialize()
{
    initialize_gpios();
    stdio_init_all();
    stdio_set_translate_crlf(&stdio_usb, false);

    // Wait for USB CDC init
    while (!stdio_usb_connected())
    {
        sleep_ms(100);
    }

    login_banner();

    alarm_pool_init_default();
    add_alarm_in_ms(UNLICENSED_USE_TIME_MS, unlicensed_timeout, (void *)0xdeadbeef, false);
}

void print_menu()
{
    puts("PLEASE SELECT AN ACTION:");
    puts("[D] ENABLE DEBUGGING TERMINAL");
    puts("[E] SHUTDOWN SESSION");
    printf("> ");
}

int getchar_ser()
{
    int retval = -1;

    while (!done && retval < 0)
    {
        retval = getchar_timeout_us(0);
    }

    if (retval > 0 && (isprint(retval) || retval == '\n' || retval == '\r'))
    {
        if (retval == '\r')
        {
            putchar('\n');
        }
        else
        {
            putchar(retval);
        }
    }

    if (done)
        retval = -1;

    return retval;
}

int get_command()
{
    print_menu();
    int retval = 0;
    int temp = 0;

    retval = getchar_ser();

    if (retval > 0)
    {
        // Clean up CR & LF chars
        temp = getchar_ser();
        if (temp != '\n' && temp != '\r')
            retval = -1;
    }

    return retval;
}

int __attribute__((noinline)) getnline(char *dst, size_t n, size_t *out)
{
    int c = 0;
    size_t total = 0;
    int retval = -1;

    if (n > 1)
    {

        c = getchar_ser();

        while (c != 0 && c != -1 && c != '\r' && c != '\n')
        {
            dst[total++] = (char)c;

            if (total == (n - 1))
                break;

            c = getchar_ser();
        }

        retval = (c == -1) ? -1 : 0;

        if (out)
            *out = total;

        dst[total] = (char)0;
    }

    return retval;
}

bool __attribute__((noinline)) is_valid_hex_string(char const *str)
{
    size_t length = strlen(str);
    if (length <= 1)
        return false;

    if ((length % 2) != 0)
        return false;

    for (size_t i = 0; i < strlen(str); ++i)
    {
        if (!isxdigit((int)str[i]))
            return false;
    }

    return true;
}

uint8_t nibble_from_hex_digit(char c)
{
    if ('0' <= c && c <= '9')
        return (uint8_t)(c - '0');
    if ('A' <= c && c <= 'F')
        return (uint8_t)((c - 'A') + 10);
    return (uint8_t)((c - 'a') + 10);
}

bool sharded_key_matches_flash_key(char const *hexstr)
{
    const uint32_t FLASH_KEY_LEN = 64;
    const uint32_t FLASH_KEY_BLOCK_HEX_LEN = 2 * FLASH_KEY_LEN;

    size_t sharded_len_hex = strlen(hexstr);
    char tmpbuf[FLASH_KEY_LEN];
    char keybuf[FLASH_KEY_LEN];

    if (sharded_len_hex < FLASH_KEY_BLOCK_HEX_LEN)
        return false;

    if ((sharded_len_hex % FLASH_KEY_BLOCK_HEX_LEN) != 0)
        return false;
        
    if (!is_valid_hex_string(hexstr))
        return false;

    memset(tmpbuf, 0, sizeof(tmpbuf));
    memcpy(keybuf, (char *)FLASH_KEY_ADDR, sizeof(keybuf));

    for (size_t i = 0; i < sharded_len_hex; i += 2)
    {
        size_t byteidx = (i % (sizeof(tmpbuf) << 1)) >> 1;
        uint8_t byteval = (uint8_t)(nibble_from_hex_digit(hexstr[i]) << 4);
        byteval |= (uint8_t)(nibble_from_hex_digit(hexstr[i + 1]));
        tmpbuf[byteidx] ^= (char)byteval;
    }

    return (strncmp(tmpbuf, keybuf, sizeof(keybuf)) == 0);
}

bool __attribute__((noinline)) debug_terminal_valid_check()
{
    char tmpbuf[1024];
    char resbuf[4060];
    size_t idx = 0;
    size_t remainder = sizeof(resbuf) - 1;

    while (!getnline(tmpbuf, sizeof(tmpbuf), NULL))
    {
        if (!tmpbuf[0])
            break;

        size_t l = snprintf(&resbuf[idx], remainder, "%s", tmpbuf);
        remainder -= l;
        idx += l;

        if (remainder <= 1)
            break;
    }

    if (remainder < (sizeof(resbuf) - 1))
    {
        return sharded_key_matches_flash_key(resbuf);
    }

    return false;
}

void __attribute__((noinline)) enable_debug_terminal()
{
    char resp[1024];

    printf("[DEBUG TERMINAL] TERMINAL KEY REQUIRED...\n");
    printf("[DEBUG TERMINAL] PLEASE ENTER SHARDED KEY IN HEX FORMAT WITH AN EMPTY LINE TO INDICATE THE END.\n");
    printf("[DEBUG TERMINAL] CONTINUE? (YES/NO): ");

    if (!getnline(resp, sizeof(resp), NULL))
    {
        if (!strcasecmp("yes", resp))
        {
            printf("[DEBUG TERMINAL] PLEASE INPUT THE SHARDED KEY ENTRIES: \n");
            if (debug_terminal_valid_check())
            {
                printf("[DEBUG TERMINAL] CORRECT SHARDED KEY SEQUENCE!\n");
                switch_debug_terminal();
            }
            else
            {
                printf("[DEBUG TERMINAL] INVALID SHARDED KEY SEQUENCE!\n");
            }
        }
        else
        {
            printf("[DEBUG TERMINAL] ABORTED SHARDED KEY RECOVERY\n");
        }
    }
}

int main()
{
    initialize();

    int command = 0;

    do
    {
        if ((command = get_command()) > 0)
        {

            int choice = tolower(command);
            switch (choice)
            {
            case 'e':
                done = true;
                break;

            case 'd':
                enable_debug_terminal();
                break;

            default:
                printf("INVALID COMMAND!\n");
                break;
            }
        }
    } while (!done);

    goodbye();

    return 0;
}