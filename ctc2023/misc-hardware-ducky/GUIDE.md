Hey there, cyber warriors! Are you ready for a challenge that will test your skills and knowledge in the world of cybersecurity? Get ready because we've got an exciting challenge for you!

As part of the Cybersecurity Capture The Flag (CTF) event, you'll be facing off against a series of cyber challenges designed to put your skills to the test. One of these challenges involves programming a Printed Circuit Board (PCB) to perform a rubber ducky attack on a centralized Windows computer via USB.

This isn't just any PCB, though. We're using the rp-micro project from GitHub, which is powered by the RP2040 microcontroller. And to make things even more exciting, the microcontroller comes preflashed with micropython, giving you the power to program the board and complete the challenge!

Your mission, should you choose to accept it, is to program the PCB to perform a series of keystrokes that will bypass User Account Control (UAC) and gain access to a file on the Windows computer that can only be read with local administrator permissions. But there's a catch – the user that is currently logged in is part of the local administrator group, and UAC is enabled. So, you'll need to find a way to bypass the UAC prompt without getting caught!

To make things more challenging, the centralized Windows computer does not have internet access. However, you will have internet access on your own system to develop the attack for the PCB. This means you can use online resources and tools to develop your attack on your own system, but you won't be able to rely on them when it comes to actually executing the attack on the Windows computer.

You'll need to make sure your attack is self-contained and doesn't require any internet connectivity to work properly. And as always, make sure to leave as few traces as possible on the target system to avoid detection.

Get creative with how you program the PCB to perform the keystrokes necessary to bypass the UAC prompt. Think outside the box and come up with a solution that will impress us and your fellow cyber warriors!

Just remember, you're not allowed to perform any other actions on the computer besides extracting the information from the file. And to keep things on the down-low, make sure to leave as few traces as possible. We don't want you getting caught in the act!

So, are you up for the challenge, cyber warriors? Let's see what you've got! Good luck and have fun!


The PCB used in this challenge is based on the rp-micro project from GitHub, which is a microcontroller board that is powered by the RP2040. This microcontroller board is preflashed with micropython, allowing you to use this language to program the board and complete the challenge.

Your objective in this challenge is to program the PCB to perform a rubber ducky attack on a centralized Windows computer via USB. The attack will exploit a vulnerability in the Windows operating system that allows a user with local administrator permissions to gain access to a file that can only be read with local administrator permissions.

The user that is currently logged in to the Windows computer is part of the local administrator group, but User Account Control (UAC) is enabled. UAC is a security feature in Windows that prompts users for permission before allowing applications to make changes to the system. This means that you will need to find a way to bypass the UAC prompt in order to gain access to the file on the computer.

Your task is to program the PCB to emulate a USB keyboard and perform a series of keystrokes that will bypass the UAC prompt and allow you to gain access to the file on the Windows computer. Once you have gained access to the file, you will need to retrieve its contents and submit them as the flag for this challenge.

You are free to choose how you program the PCB to perform the keystrokes necessary to bypass the UAC prompt, but be creative and think outside the box! We wish you the best of luck in this challenge and hope you have fun testing your skills and knowledge in the world of cybersecurity.


  
  
The RP2040 is a microcontroller developed by the Raspberry Pi Foundation. It's a powerful and versatile chip that has become popular for a wide range of applications, from hobbyist projects to commercial products. The RP2040 features dual ARM Cortex-M0+ cores running at up to 133MHz, as well as a range of I/O peripherals and connectivity options, including USB, UART, SPI, and I2C.

The rp-micro project is a set of hardware designs and software tools that are specifically tailored to the RP2040. It includes a range of PCB designs, including a small form factor board that is perfect for prototyping and development, as well as larger boards for more complex projects. The project also includes a set of software libraries and examples that make it easy to get started with the RP2040 and start building your own projects.

The rp-micro project is open source and available on GitHub, so anyone can contribute to the project and use it to build their own hardware projects. It's a great resource for anyone who wants to get started with the RP2040 and explore its capabilities, and it's also a great platform for experimenting with cybersecurity challenges, like the one we've set up for the CTF event!
