def print_banner():
	print("""
 ██████ ████████  ██████ ██████   ██████  ██████  ██████  
██         ██    ██           ██ ██  ████      ██      ██ 
██         ██    ██       █████  ██ ██ ██  █████   █████  
██         ██    ██      ██      ████  ██ ██           ██ 
 ██████    ██     ██████ ███████  ██████  ███████ ██████  
""")

def print_welcome():
	print("Welcome to CTC 2023 - Aquatic Adventure!")
	print("Using this ducky you will be able to solve a number of challenges. Refer to the CTF portal to find out more.")
	print("After the event, you can take the ducky home. Good luck and have fun!")
	print()
