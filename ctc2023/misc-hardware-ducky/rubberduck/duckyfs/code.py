import time
import banner

# sleep at the start to allow the device to be recognized by the host computer
time.sleep(1)

# do whatever we want to do :)
banner.print_banner()
banner.print_welcome()
