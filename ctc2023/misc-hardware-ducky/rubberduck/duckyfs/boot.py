import supervisor

# disable auto-reload when changing python files
supervisor.runtime.autoreload = False
