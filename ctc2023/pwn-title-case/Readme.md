# Status: OK
# Title Case

## Synopsis

The challenge itself only consists of a single line:

`eval(input.title())`

`title()` places the string in 'Title Case'....

## Walkthrough

This challenge was based on: https://peps.python.org/pep-0672/

𝓹rint(𝗼pen(𝗰hr(47)+𝗰hr(102)+𝗰hr(108)+𝗰hr(97)+𝗰hr(103)).𝗋ead())

