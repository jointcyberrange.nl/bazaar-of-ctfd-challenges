"use client";

import { Inter } from 'next/font/google'

const inter = Inter({ subsets: ['latin'] })

import Admin from "../../components/Admin";
import Feed from "../../components/Feed"


export default function Home() {
    return (
        <div className="bg-gray-900">
            <Admin> 
                <Feed />
            </Admin>
        </div>
    )
}