# Status: OK
source: https://gitlab.com/jointcyberrange.nl/challenges-2023/-/tree/main/challenges/ctc2023-for-blue-whale?ref_type=heads


Synopsis

After extracting the gzipped tar the file to investigate is bluewhale.png. Viewing the file with any image viewer will show a picture of a blue whale. The picture seems normal and on the picture itself it cannot be derived what the blue whale swallowed. The fileformat appears to be a normal PNG file. However, this file is created making use of the feature that any data after the last IEND chunk is ignored by all image viewers. The acropalypse vulnerability is discovered by David Buchanan and Simon Aarons (https://en.wikipedia.org/wiki/ACropalypse). After applying file forensics techniques it can be discovered that the original picture was cropped and after the last IEND PNG chunk there is a hidden JPG picture. That picture contains a swallowed rubber ducky and the flag text

Walkthrough

tar-xf bluewhale.tar.gz to get the bluewhale.png file
view the file with any image viewer
check the file format: '$ file bluewhale.png'  ->  " bluewhale.png: PNG image data, 1658 x 733, 8-bit/color RGBA, non-interlaced. "
check with strings command ' $ strings bluewhale.png'  -> output will not lead to a flag
check metadata with $ exiftool -> normal metadata output but you may notice the warning " [minor] Trailer data after PNG IEND chunk " which givea a good clue what might be going on
use a hex editor to inspect the file in depth
use reference documentation about file signatures to understand PNG file formats and other file formats for example from corkami https://github.com/corkami/pics/blob/master/posters/README.md, the official RFC specification of PNG https://www.rfc-editor.org/rfc/rfc2083, other filesignature references like https://en.wikipedia.org/wiki/List_of_file_signatures, https://www.garykessler.net/library/file_sigs.html

recovery option 1: use a hex editor like NEO or ghex and look for file signatures, discover a ' FF D8 ' as start of a hidden JPG and trailing ' FF D9 ' in hex. Carve the bytes out and save the bytes as a .jpg extension. View the jpg to find a cropped part of the image with rubber ducky and flag text
recovery option 2: use a tool like binwalk to find hidden files in files. ' $ binwalk bluewhale.png -> output will show that it is PNG data but also Zlib compressed data and JPG data,  binwalk also has extraction options, try ' binwalk --dd='.*' bluewhale.png' or more specific ' $ binwalk --dd='jpeg:jpg' bluewhale.png '  this will extract files with zlib bytes but also a file with a filename like ' 1B9B33' , rename that file to a file with .jpg extension, open with a image viewer and find the rubberducky and flag text