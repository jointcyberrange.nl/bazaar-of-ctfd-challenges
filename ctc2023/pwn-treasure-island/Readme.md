# Status: DOCKER-BUILD-FAILS
# Treasure Island

## Synopsis

The challenge allows you to open files and close file descriptors. The idea of the challenge is that you can close stdout (due to a buffer underrun). The next time a file will be opened it will receive fd 1 from the kernel; as a consequence all printf() statements will go to this file rather than to the normal stdout.

## Walkthrough

1. go to close treasure chest
2. enter -7, which causes a underrun to someplace which has a 1 at the
   location of treasure.fd.
3. go to open treasure chest
4. enter 0 (flag), this will open the key file (which will get fd 1)
5. enter a random password. Since the key file is already read, the current
   pointer will be at the end of the file (meaning all writes will go to the
   end of the file as well), naturally this is not good.
6. go to 2, look inside treasure chest
7. enter -8, a second underrun to another fd=1, since the close function
   will set the fd in the treasure struct to 0. The function will first do a
   read, and then an lseek to the start of the file. It will then do a print
   statement, making this the key.
8. enter 99 to close the program
9. restart the challenge and open the flag treasure, the key should be
   'You can always come back to look at it a second time'

Source: https://gitlab.com/jointcyberrange.nl/challenges-2023/-/tree/main/challenges/ctc2023-pwn-treasure-island?ref_type=heads