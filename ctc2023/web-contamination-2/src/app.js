//CTF{th1s_d0c_1s_pr1v4t3}

const express = require('express');
const child_process = require('child_process');
const fs = require('fs');
const _ = require('lodash');
const { dirname } = require('path');

const PORT = 8000
const HOST = '0.0.0.0';

// App
const app = express();
app.use(express.json());

app.get("/download", (req, res) => {
  let app_dir = dirname(require.main.filename);
  res.send(fs.readFileSync(app_dir+"/shared_files/"+req.query.file));
})

app.get("/sysinfo", (req, res) => {
  res.send(child_process.execSync("uname", ["-a"]));
});

app.post("/set_config", (req, res) => {
  //Set default config, then merge with config keys sent by user
  let default_conf = {"test_default_key_1":"test_value_1", "test_default_key_2":"test_value_2"};
  let user_conf = req.body
  let final_conf = _.merge(default_conf, user_conf)
  //TODO: Actually set config 
  //Return final config
  res.send(final_conf)
});


app.listen(PORT, HOST, () => {
  console.log(`Running on http://${HOST}:${PORT}`);
});