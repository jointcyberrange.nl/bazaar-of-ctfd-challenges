import socket
import sys
import pickle
import os
import base64
import time
import traceback
import contextlib
import io
import stackprinter

class Command:
    def execute(self):
        pass

class HelpCommand(Command):
    def execute(self):
        help_menu = """
\n
-------------------------------------------------------------------------

                 ██╗░░██╗███████╗██╗░░░░░██████╗░
                 ██║░░██║██╔════╝██║░░░░░██╔══██╗
                 ███████║█████╗░░██║░░░░░██████╔╝
                 ██╔══██║██╔══╝░░██║░░░░░██╔═══╝░
                 ██║░░██║███████╗███████╗██║░░░░░
                 ╚═╝░░╚═╝╚══════╝╚══════╝╚═╝░░░░░
-------------------------------------------------------------------------

Welcome to the Deep-See exploration submarine control system! 

Here are the available commands:

- move <direction>: Move the submarine in the specified direction by one unit.
- surface: Surface the submarine to sea level.
- search <target>: Search the surrounding area for the specified target (e.g., wreckage, sea life, treasure).
- sonar: Activate the submarine's sonar system to detect nearby objects.
- status: Display the current status of the submarine, including its location, depth, and fuel level.

Use the following format to enter commands: <command> <argument>.
For example, to move the submarine north, enter: move north.

To see this help menu again, enter: help.
\n\n\n
"""
        return f"{help_menu}"

class FlagCommand(Command):
    def execute(self):
        flag = '''
                                   ___
                               ,-""   `.
                             ,'  _   e )`-._
                            /  ,' `-._<.===-'
                           /  /
                          /  ;
              _.--.__    /   ;
 (`._    _.-""       "--'    |
 <_  `-""                     \\
  <`-         Kwak Kwak        :
   (__   <__.                  ;
     `-.   '-.__.      _.'    /
        \\      `-.__,-'    _,'
         `._    ,    /__,-'
            ""._\\__,'< <____
                 | |  `----.`.
                 | |        \\ `.
                 ; |___      \\-``
                 \\   --<
                  `.`.<
                    `-' 
'''
        return f"{flag}"

class DeepSeeCommand(Command):
    def execute(self):
        ascii_art = """
-------------------------------------------------------------------------

        ██████████████████████████████████████████████████████
        █▄─▄▄▀█▄─▄▄─█▄─▄▄─█▄─▄▄─███▀▀▀▀▀████─▄▄▄▄█▄─▄▄─█▄─▄▄─█
        ██─██─██─▄█▀██─▄█▀██─▄▄▄████████████▄▄▄▄─██─▄█▀██─▄█▀█
        ▀▄▄▄▄▀▀▄▄▄▄▄▀▄▄▄▄▄▀▄▄▄▀▀▀▀▀▀▀▀▀▀▀▀▀▀▄▄▄▄▄▀▄▄▄▄▄▀▄▄▄▄▄▀

-------------------------------------------------------------------------
Welcome aboard the Deep-See exploration submarine, where adventure and 
treasure await! As you embark on this underwater journey, you will be 
under the expert guidance of Captain Cucumber, also known as Pickle, 
a seasoned sailor with a sharp wit and a knack for finding hidden gems.

Your mission is simple: explore the depths of the ocean and unearth the 
long-lost treasures that lie hidden within. Equipped with the latest 
technology and state-of-the-art equipment, the Deep-See will take you 
on a voyage like no other, revealing the secrets of the sea and uncovering 
priceless treasures along the way.

As you navigate the underwater world, you will encounter a myriad of 
challenges and obstacles, but fear not! With Captain Cucumber at the helm, 
you are in safe hands. So sit back, relax, and prepare for the adventure
of a lifetime, as you set sail with the Deep-See exploration submarine!
-------------------------------------------------------------------------
\n \n \n
"""
        return f"{ascii_art}"

class MoveCommand(Command):
    def execute(self):
        return 'The submarine has been directed towards the specified direction. \n'

class SurfaceCommand(Command):
    def execute(self):
        return 'The submarine has been commanded to reach surface. \n'

class SearchCommand(Command):
    def execute(self):
        return 'The search sequence has been started. \n'

class SonarCommand(Command):
    def execute(self):
        return 'The sonar system has been activated. \n'

class StatusCommand(Command):
    def execute(self):
        return 'Excellent news, all systems on the submarine are functioning perfectly. \n'

class HiddenCommand(Command):
    def execute(self, conn, data):
        input_bytes = data.strip().split(b' ', 1)[1]
        try:
            captured_output = io.StringIO()
            with contextlib.redirect_stdout(captured_output):     
                input_bytes = base64.urlsafe_b64decode(input_bytes)
                input_obj = pickle.loads(input_bytes)
            
            captured_string = captured_output.getvalue()
            conn.send(captured_string.encode())         
            return True
        except:
            stack = stackprinter.format()
            conn.send(stack.encode())
            return False

class ProgressBarCommand50(Command):
    def execute(self):
        # Generate a progress bar string (e.g. "Progress: [##############         ] 50%")
        progress = "Progress: [##############         ] 50% \n"
        return progress

class ProgressBarCommand75(Command):
    def execute(self):
        progress = "Progress: [##################     ] 75% \n"
        return progress

class ProgressBarCommand100(Command):
    def execute(self):
        progress = "Progress: [#######################] 100% \n \n \n"
        return progress

def run_service():
    # Create a TCP/IP socket
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    # Bind the socket to a specific address and port
    server_address = ('localhost', 6661)
    print('Starting up on %s port %s' % server_address)
    sock.bind(server_address)

    # Listen for incoming connections
    sock.listen(1)

    while True:
        # Wait for a connection
        print('Waiting for a connection...')
        conn, client_address = sock.accept()
        os.dup2(conn.fileno(),0);
        os.dup2(conn.fileno(),1);
        os.dup2(conn.fileno(),2);
        print('Connection from', client_address)
        status = ProgressBarCommand50().execute()
        conn.send(status.encode())
        status = ProgressBarCommand75().execute()
        time.sleep(1)
        conn.send(status.encode())
        time.sleep(1)
        status = ProgressBarCommand100().execute()
        conn.send(status.encode())
        status = DeepSeeCommand().execute()
        conn.send(status.encode())

        # Handle the connection
        try:
            while True:
                data = conn.recv(1024)
                if not data:
                    break
                
                command = data.decode().strip()
                
                if command.lower().startswith("move "):
                    # Split the command into words
                    words = command.split()
                    # Check if the second word is a valid integer
                    if len(words) == 2:        
                        try:
                            # Try to convert the second word to an integer
                            distance = int(words[1])
                            # TODO: test the implementation of the >>> "server2server" <<< command
                            # Execute the MoveCommand with the distance argument
                            status = MoveCommand().execute()
                            conn.send(status.encode())
                        except:
                            stack = stackprinter.format()
                            conn.send(stack.encode())
                            conn.send(" \n \n Invalid move command. Please use 'move <integer>'. \n \n".encode())
                    else:
                        # Send an error message back to the client
                        conn.send("Invalid move command. Please use 'move <integer>'.".encode())
                
                elif command.lower() == 'surface':
                    status = SurfaceCommand().execute()
                    conn.send(status.encode())
                
                elif command.lower().startswith("search "):
                    words = command.split()
                    if words[1] == "treasure":
                        status = '''\n 
----------------------------------------------------------------------------------------------

           ####   ####  #    #  ####  #####    ##   #####  ####  
          #    # #    # ##   # #    # #    #  #  #    #   #      
          #      #    # # #  # #      #    # #    #   #    ####  
          #      #    # #  # # #  ### #####  ######   #        # 
          #    # #    # #   ## #    # #   #  #    #   #   #    # 
           ####   ####  #    #  ####  #    # #    #   #    ####  

----------------------------------------------------------------------------------------------

Congratulations, captain! 

You've stumbled upon a secret menu that can help you on your quest to find the treasure. 

The "server2server" command is your key to communicating with other submarines and teaming up to find the treasure you seek. 
But with great power comes great responsibility. Use the "server2server" command wisely, as it grants you root access to all 
of the submarine's secrets, even those known only to the Captain Picle.

But fear not, as the power of the "server2server" command will also lead you to a flag that can change your destiny. 
This flag is hidden away in a special vault at the following location within the submarine's operating system: 

/home/captainpickle/secretvault/flag.txt

Once you've found the flag and entered the correct sequence, you'll be granted a new rank: "Oceanic Overlord"
With this new status, you'll have access to even more resources and information that can aid you on your quest 
for the final treasure. Use the "server2server" command wisely and find the flag that awaits you. 

May the winds and waves carry you to glory, dear adventurer!
---------------------------------------------------------------------------------------------- \n'''
                        conn.send(status.encode())
                    else:
                        status = SearchCommand().execute()
                        conn.send(status.encode())
                elif command.lower() == 'sonar':
                    status = SonarCommand().execute()
                    conn.send(status.encode())
                
                elif command.lower()== 'status':
                    status = StatusCommand().execute()
                    conn.send(status.encode())
                
                elif command.lower() == 'help':
                    status = HelpCommand().execute()
                    conn.send(status.encode())
                
                elif command.lower().startswith("server2server "):
                    hidden_output = HiddenCommand().execute(conn, data)                    
                    if hidden_output == True:
                        status = FlagCommand().execute()
                        conn.send(status.encode())
                    elif hidden_output == False:
                        status = '''
\n \n
-------------------------------------------------------------------
                    Aye Aye Captain! Try harder!
-------------------------------------------------------------------
\n \n'''
                        conn.send(status.encode())
                    print(hidden_output)
                
                else:
                    status = "Command not recognized. Please try again.\n\n\n"
                    conn.send(status.encode())                    
        finally:
            # Clean up the connection
            conn.close()


if __name__ == '__main__':
    run_service()