# Status: DOCKER-COMPOSE-MISSING

Source: <https://gitlab.com/jointcyberrange.nl/challenges-2023/-/tree/main/challenges/ctc2023-misc-deepsee-submarine?ref_type=heads>

Walkthrough

Begin by using nmap to scan for open ports on the target system. Once you identify that port 6666 is open, use nmap to confirm that there is a server listening on that port.

Connect to the server using the "nc" command.

Once connected, play around with the service to see what kind of commands are available.

Note that the "move" command expects integers as input. If you provide anything else, you will receive an error message.

Take a closer look at the error message to find a comment in the stack trace that says "TODO". This indicates that there is a hidden command available called "server2server". This command is not listed in the help menu.

Experiment with the server2server command to determine what type of input it expects. If you provide incorrect input, you can analyze the resulting stack trace to gain insight into what is expected. In this case, you will need to provide the input in base64-pickled format.

Once you have determined the correct input format (pickled + base64) for the server2server command, craft a command that will read the flag file located at "/home/captainpickle/secretvault/flag.txt" and display its contents to you.

When you run the command, you should see the contents of the flag file displayed in the terminal. Congratulations, you have successfully completed the challenge!
