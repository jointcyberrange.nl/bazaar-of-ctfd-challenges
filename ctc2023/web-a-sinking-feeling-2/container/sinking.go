package main

import (
	"fmt"
	"log"
	"math/rand"
	"net"
)

type Sensordata struct {
	Depth int
	Temp  int
}

func (s *Sensordata) update() {
	// Slight bias towards going deeper
	s.Depth += rand.Intn(61) - 29
	if s.Depth < 0 {
		s.Depth = 0
	}
	if s.Depth > 5010 {
		s.Depth = 5010
	}

	s.Temp += rand.Intn(17) - 8
	if s.Temp < -50 {
		s.Temp = -50
	}
	if s.Temp > 193 {
		s.Temp = 193
	}
}

func handle(conn net.Conn, s *Sensordata) {
	defer conn.Close()
	s.update()

	d := float64(s.Depth) / 100
	t := float64(s.Temp) / 10
	p := float64(s.Depth) / 1000

	content := fmt.Sprintf("Depth: <b>%.2f</b> meters<br>\nTemperature: <b>%.1f</b> °C<br>\nPressure: <b>%.2f</b> bar<br>\n", d, t, p)
	b := []byte(content)

	if _, err := conn.Write(b[0:cap(b)]); err != nil {
		log.Fatalln("Unable to write data")
	}
}

func main() {
	s := Sensordata{300, 50}

	listener, err := net.Listen("tcp", ":9101")
	if err != nil {
		log.Fatalf("Unable to bind port")
	}
	log.Println("Listening on 0.0.0.0:9101")
	for {
		conn, err := listener.Accept()
		if err != nil {
			log.Fatalln("Unable to accept connection")
		}

		go handle(conn, &s)
	}
}
