FROM ubuntu:22.04

RUN apt-get update \
    && apt-get install -y \
        git \
        cmake \
        gcc-arm-none-eabi \
        libnewlib-arm-none-eabi \
        build-essential \
        python3 \
    && rm -rf /var/lib/apt/lists/*

RUN git clone --depth 1 --branch 1.3.0 --recurse-submodules https://github.com/raspberrypi/pico-sdk.git /opt/pico-sdk

ENV PICO_SDK_PATH=/opt/pico-sdk

WORKDIR /source

CMD ["/bin/sh", "-c", "cmake -Wno-dev -B /build -S /source && cmake --build /build"]