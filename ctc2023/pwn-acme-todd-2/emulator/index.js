// Code inspired by rp2040js-circuitpython @ https://github.com/wokwi/rp2040js-circuitpython/blob/main/src/index.js
const { bootromB2 } = require('./bootrom');
const { RP2040, USBCDC, GPIOPinState } = require('rp2040js');
const { decodeBlock } = require('uf2');
const { openSync, closeSync, readSync } = require('fs');
const { spawnSync } = require('child_process');

const LOG_NAME = 'RP2040';
const FLASH_START_ADDRESS = 0x10000000;
const RAM_START_ADDRESS = 0x20000000;
const DPRAM_START_ADDRESS = 0x50100000;
const SIO_START_ADDRESS = 0xd0000000;

class StrictRP2040 extends RP2040 {
    constructor() { super() }

    readUint32(address) {
        address = address >>> 0; // round to 32-bits, unsigned
        if (address & 0x3) {
          this.logger.error(
            LOG_NAME,
            `Read from address ${address.toString(16)}, which is not 32 bit aligned`
          );
        }
    
        const { bootrom } = this;
        if (address < bootrom.length * 4) {
          return bootrom[address / 4];
        } else if (
          address >= FLASH_START_ADDRESS &&
          address < FLASH_START_ADDRESS + this.flash.length
        ) {
          return this.flashView.getUint32(address - FLASH_START_ADDRESS, true);
        } else if (address >= RAM_START_ADDRESS && address < RAM_START_ADDRESS + this.sram.length) {
          return this.sramView.getUint32(address - RAM_START_ADDRESS, true);
        } else if (
          address >= DPRAM_START_ADDRESS &&
          address < DPRAM_START_ADDRESS + this.usbDPRAM.length
        ) {
          return this.usbDPRAMView.getUint32(address - DPRAM_START_ADDRESS, true);
        } else if (address >>> 12 === 0xe000e) {
          return this.ppb.readUint32(address & 0xfff);
        } else if (address >= SIO_START_ADDRESS && address < SIO_START_ADDRESS + 0x10000000) {
          return this.sio.readUint32(address - SIO_START_ADDRESS);
        }
    
        const peripheral = this.findPeripheral(address);
        if (peripheral) {
          return peripheral.readUint32(address & 0x3fff);
        }

        // Allow XIP_CTRL_* reads
        if (address === 0x14000004 || address === 0x14002000) {
            return 0xffffffff;
        }

        // error instead of warn
        this.logger.error(LOG_NAME, `Read from invalid memory address: ${address.toString(16)}`)
        return 0xffffffff;
    }

    writeUint32(address, value) {
        address = address >>> 0;
        const { bootrom } = this;
        const peripheral = this.findPeripheral(address);
        if (peripheral) {
          const atomicType = (address & 0x3000) >> 12;
          const offset = address & 0xfff;
          peripheral.writeUint32Atomic(offset, value, atomicType);
        } else if (address < bootrom.length * 4) {
          bootrom[address / 4] = value;
        } else if (
          address >= FLASH_START_ADDRESS &&
          address < FLASH_START_ADDRESS + this.flash.length
        ) {
          this.flashView.setUint32(address - FLASH_START_ADDRESS, value, true);
        } else if (address >= RAM_START_ADDRESS && address < RAM_START_ADDRESS + this.sram.length) {
          this.sramView.setUint32(address - RAM_START_ADDRESS, value, true);
        } else if (
          address >= DPRAM_START_ADDRESS &&
          address < DPRAM_START_ADDRESS + this.usbDPRAM.length
        ) {
          const offset = address - DPRAM_START_ADDRESS;
          this.usbDPRAMView.setUint32(offset, value, true);
          this.usbCtrl.DPRAMUpdated(offset, value);
        } else if (address >= SIO_START_ADDRESS && address < SIO_START_ADDRESS + 0x10000000) {
          this.sio.writeUint32(address - SIO_START_ADDRESS, value);
        } else if (address >>> 12 === 0xe000e) {
          this.ppb.writeUint32(address & 0xfff, value);
        } else if (address === 0x14000004 || address === 0x14002000) {
            // Allow XIP_CTRL_* writes
        }
        else {
          // error instead of warn
          this.logger.error(LOG_NAME, `Write to undefined address: ${address.toString(16)}`);
        }
      }
}

// Do nothing logger - leak 
class EmptyLogger {
    constructor(dieOnError = true) {
        this.dieOnError = dieOnError;
    }
    debug(c, m) {  }
    warn(c, m) { }
    error(c, m) {
        console.log(`${c}: ${m}`);
        if (this.dieOnError) {
            process.exit(2);
        }
    }
    info(c, m) { }
}

const loadUF2 = (mcu, filepath) => {
    const file = openSync(filepath, 'r');
    const buffer = new Uint8Array(512);
    while (readSync(file, buffer) === buffer.length) {
        const block = decodeBlock(buffer);
        const { flashAddress, payload } = block;
        mcu.flash.set(payload, flashAddress - FLASH_START_ADDRESS);
    }
    closeSync(file);
}

const loadROMAndUF2 = (mcu, bootrom, uf2_path) => {
    mcu.loadBootrom(bootrom);
    loadUF2(mcu, uf2_path);
}

const initIO = (mcu) => {
    const cdc = new USBCDC(mcu.usbCtrl);

    for (const stream of [process.stdout, process.stdin]) {
      stream?._handle?.setBlocking?.(true);
    }

    cdc.onSerialData = (buffer) => {
        process.stdout.write(buffer);
    };

    if (process.stdin.isTTY) {
      process.stdin.setRawMode(true);
    }
    process.stdin.on('data', (chunk) => {
        for (const byte of chunk) {
            cdc.sendSerialByte(byte);
        }
    });

    const ACME_TERMINAL_ENABLE = mcu.gpio[4];
    const ACME_TERMINAL_READY = mcu.gpio[5];
    const ACME_SHUTDOWN = mcu.gpio[6];
    
    // Set TERMINAL READY
    ACME_TERMINAL_READY.setInputValue(true);

    // Set shutdown
    let disableShutdown = ACME_SHUTDOWN.addListener((value, oldValue) => {
        if (value === GPIOPinState.High && oldValue === GPIOPinState.Low) {
            process.exit(0);
        }
    });

    ACME_TERMINAL_ENABLE.addListener((value, oldValue) => {
        if (value === GPIOPinState.High && oldValue === GPIOPinState.Low) {
            mcu.stop();
            disableShutdown();
            spawnSync('/opt/debug_terminal', [], {stdio: 'inherit'});
            process.exit(0);
        }
    });
}

const startMCU = (mcu) => {
    mcu.core.PC = FLASH_START_ADDRESS;
    mcu.logger = new EmptyLogger(true);
    mcu.execute();
}

const run = () => {
    const mcu = new StrictRP2040();
    const UF2_PATH = `${__dirname}/challenge.uf2`;
    loadROMAndUF2(mcu, bootromB2, UF2_PATH)
    loadUF2(mcu, `${__dirname}/flag1.uf2`);
    initIO(mcu);
    try {
        startMCU(mcu);
    } catch (error) {
        process.exit(1);
    }
}

run();







