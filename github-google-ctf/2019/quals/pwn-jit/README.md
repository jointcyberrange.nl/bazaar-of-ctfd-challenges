# Status: OK
Source: https://github.com/google/google-ctf/tree/main/2019/quals/pwn-jit

Changes:
Old flag:CTF{8röther_m4y_1_h4v3_söm3_nümb3r5}
New flag: CTF{8rother_m4y_1_h4v3_som3_numb3r5}
- Old flag gave errors