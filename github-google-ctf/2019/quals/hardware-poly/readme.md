# Status: DOCKER-BUILD-FAILS
Source: https://github.com/google/google-ctf/tree/main/2019/quals/hardware-poly

Changes: Python2 > Python3 (Dockerfile)

Error: 
------
Dockerfile:18
--------------------
  16 |
  17 |     RUN pacman -Syy
  18 | >>> RUN pacman -Sy --noconfirm python2
  19 |     RUN pacman -Sy --noconfirm socat
  20 |     RUN pacman -Sy --noconfirm qemu qemu-arch-extra gnu-netcat
--------------------
ERROR: failed to solve: process "/bin/sh -c pacman -Sy --noconfirm python2" did not complete successfully: exit code: 1