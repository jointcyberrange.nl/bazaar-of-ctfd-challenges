# Status: NOT-WORKING
Not sure if and how this should be used or deployed...

Source: [https://github.com/google/google-ctf/tree/main/2020/quals/web-littlethings-public](https://github.com/google/google-ctf/tree/main/2020/quals/web-littlethings-public)

# Public things

This is a support application for `web-littlethings-fixed`. Exactly the same as `web-pasteurize`, but with different database.
