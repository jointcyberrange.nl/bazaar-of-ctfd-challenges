# Status: NOT-WORKING
Not sure if and how this should be used or deployed...

Source: [https://github.com/google/google-ctf/tree/main/2020/quals/web-typeselfsub-support](https://github.com/google/google-ctf/tree/main/2020/quals/web-typeselfsub-support)

# Not for the scoreboard

This domain is used in the web-typeselfsub challenge (now called Tech Support)

Instructions on how to run are in the ../web-typeselfsub folder
