# Status: DESCRIPTION-MISSING

Source: [https://github.com/google/google-ctf/tree/main/2020/quals/pwn-echo](https://github.com/google/google-ctf/tree/main/2020/quals/pwn-echo)

## Changes made:
- Added `.c` file extensions to `launcher` and `entry` in `Dockerfile`
