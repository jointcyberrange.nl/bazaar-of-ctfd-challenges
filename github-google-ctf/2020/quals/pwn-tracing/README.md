# Status: DESCRIPTION-MISSING DOCKERFILE-MISSING

Source: [https://github.com/google/google-ctf/tree/main/2020/quals/pwn-tracing](https://github.com/google/google-ctf/tree/main/2020/quals/pwn-tracing)

Run with `RUST_LOG=debug cargo run --bin server 'CTF{1BitAtATime}'`

Exploit with `RUST_LOG=info cargo run --bin exploit`
