# Status: OK

Source: [https://github.com/google/google-ctf/tree/main/2020/quals/reversing-dotnet](https://github.com/google/google-ctf/tree/main/2020/quals/reversing-dotnet)

This is a .NET executable targeting the .NET Framework v4.5.2.
