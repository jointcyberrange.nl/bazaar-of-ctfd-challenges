# Status: OK

Source: [https://github.com/google/google-ctf/tree/main/2017/quals/2017-misc-anon](https://github.com/google/google-ctf/tree/main/2017/quals/2017-misc-anon)

## Changes made:

- `RUN apt-get install -y netcat` removed from `Dockerfile` (no installation candidate)