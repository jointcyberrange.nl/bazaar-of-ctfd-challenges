# Status: OK

Source: [https://github.com/google/google-ctf/tree/main/2017/quals/2017-pwn-primary](https://github.com/google/google-ctf/tree/main/2017/quals/2017-pwn-primary)

## Changes made:

- Updated `ubuntu` version from 14.10 to 23.10 in `Dockerfile`