# Status: OK

Source: [https://github.com/google/google-ctf/tree/main/2017/quals/2017-crypto-selfhash](https://github.com/google/google-ctf/tree/main/2017/quals/2017-crypto-selfhash)

## Changes made:

- Removed `repository/` from `Dockerfile` `ADD` instructions