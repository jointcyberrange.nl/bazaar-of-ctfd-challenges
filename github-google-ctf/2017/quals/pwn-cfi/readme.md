# Status: OK

Source: [https://github.com/google/google-ctf/tree/main/2017/quals/2017-pwn-cfi](https://github.com/google/google-ctf/tree/main/2017/quals/2017-pwn-cfi)

## Changes made:

- Updated python version from "2.7" to "3.5" in `Dockerfile`