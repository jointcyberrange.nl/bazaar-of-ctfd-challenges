# Status: DOCKER-BUILD-FAILS

Source: [https://github.com/google/google-ctf/tree/main/2017/quals/2017-re-johnny-boy](https://github.com/google/google-ctf/tree/main/2017/quals/2017-re-johnny-boy)

## Changes made:

- Updated `ubuntu` version from 16.10 to 23.10 in `Dockerfile`

## Problems:

- `apt-get install -y wget xz-utils openjdk-8-jre xvfb python python-pil` fails => cannot build image