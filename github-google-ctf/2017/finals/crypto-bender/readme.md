# Status: MISSING-FILES

Source: [https://github.com/google/google-ctf/tree/main/2017/finals/2017-finals-crypto-bender](https://github.com/google/google-ctf/tree/main/2017/finals/2017-finals-crypto-bender)

## Problems:

- Missing file `metadata.json` in source repository => unable to build docker image