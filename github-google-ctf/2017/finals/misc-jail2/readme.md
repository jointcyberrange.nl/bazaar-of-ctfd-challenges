# Status: OK

Source: [https://github.com/google/google-ctf/tree/main/2017/finals/2017-finals-misc-jail2](https://github.com/google/google-ctf/tree/main/2017/finals/2017-finals-misc-jail2)

## Changes made:

- Updated ubuntu version from 17.04 to 23.10 in `Dockerfile`