# Status: OK

Source: [https://github.com/google/google-ctf/tree/main/2017/finals/2017-finals-misc-jail1](https://github.com/google/google-ctf/tree/main/2017/finals/2017-finals-misc-jail1)

## Changes made:

- Updated ubuntu version from 17.04 to 23.10 in `Dockerfile`