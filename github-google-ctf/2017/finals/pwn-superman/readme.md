# Status: OK

Source: [https://github.com/google/google-ctf/tree/main/2017/finals/2017-finals-pwn-superman](https://github.com/google/google-ctf/tree/main/2017/finals/2017-finals-pwn-superman)

## Changes made:

- Corrected for `attachments` now called `files` in `Dockerfile`