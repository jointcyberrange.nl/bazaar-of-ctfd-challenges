# Status: OK

Source: github.com/google/google-ctf/tree/main/2023/quals/rev-zermatt

ZerMatt
Participants receive an obfuscated lua file. The goal is to deobfuscate it, and get the flag.

See src/main.lua for the unobfuscated source.