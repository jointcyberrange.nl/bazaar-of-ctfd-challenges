# Status: OK

Source: github.com/google/google-ctf/tree/main/2023/quals/crypto-lcg

Least Common Genominator?
Flavor Text
Someone used this program to send me an encrypted message but I can't read it! It uses something called an LCG, do you know what it is? I dumped the first six consecutive values generated from it but what do I do with it?!

Solution
see solution/README.md

Repository Structure
Attachments: What is sent to the players
Src: Generate challenge, modify values used in config, or you can move the solver script into the attachments repo and run it to get the flag.

# Copyright 2021 Google LLC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# Human readable task name