# Status: OK

Source: github.com/google/google-ctf/tree/main/2023/quals/rev-flangton

Langton challenge
It's a classic reversing challenge. The binary asks for the flag, and tells you if it's correct. Spoilers inside the challenge directory.