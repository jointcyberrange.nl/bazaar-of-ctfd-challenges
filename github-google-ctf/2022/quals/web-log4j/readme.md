# Status: FLAG-MISSING DESCRIPTION-MISSING
Source: [https://github.com/google/google-ctf/tree/main/2022/quals/web-log4j](https://github.com/google/google-ctf/tree/main/2022/quals/web-log4j)

## Problems:

- Missing flag in `challenge.yml` (not mentioned in source repository)
- Missing description in `challenge.yml` (not mentioned in source repository)