# Status: FLAG-MISSING DESCRIPTION-MISSING
Source: [https://github.com/google/google-ctf/tree/main/2022/quals/web-postviewer](https://github.com/google/google-ctf/tree/main/2022/quals/web-postviewer)

## Problems:

- Missing flag in `challenge.yml` (missing in source repository)
- Missing description in `challenge.yml` (missing in source repository)