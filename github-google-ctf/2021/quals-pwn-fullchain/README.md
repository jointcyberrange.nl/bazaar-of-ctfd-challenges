# Status: DOCKER-BUILD-FAILS

# Fullchain

TL;DR: players provide a HTML page, we open it in Chromium in a VM. Players have to
chain 3 bugs to become root in the VM.

Chromium is built from commit 1be58e78c7ec6603d416aed4dfae757334cd4e1e

## How to deploy on kCTF

```sh
kctf chal start
```

## How to rebuild the challenge

**!!! This does not rebuild Chromium, see below for that !!!**

```sh
cd challenge
make
```

We don't store the VM image and built Chromium in Git because they are too big. If
you rebuild the challenge you will also need to recreate the attachment archive.
`challenge/make_attachment.py` creates the zip file with all the attachments.
You will still need to upload it to GCS and update `Dockerfile` and `metadata.yaml` with the new URLs.

If you rebuild/change the kernel or the kernel module you might need to update the kernel exploit
in healthcheck (`healthcheck/kernel_exploit.c`) (see instructions near the top of the file).
To rebuild the kernel exploit use

```sh
cd healthcheck
make
```

## How to rebuild Chromium

Good luck! /s

**Warning**: this takes a while (about 1h if you have to check out the source the source on my corp workstation) even with Goma.

**Warning**: Updating the exploit might also take a while, assuming that it doesn't break entirely.

`challenge/update_chromium.sh` is adapted from sroettger@'s [script](https://team.git.corp.google.com/ctfcompetition/2020-challenges-quals/+/refs/heads/master/sandbox-teleport/update_chrome.sh)
from last year's edition. It checks out the source code at the specified version and builds it.
You need to [install `depot_tools`](https://chromium.googlesource.com/chromium/src/+/refs/heads/main/docs/linux/build_instructions.md#install)
and [set up Goma](https://www.chromium.org/developers/gn-build-configuration) before running that script.

Goma only works on Corp workstations. If you want to build Chromium on another machine you can do that by disabling Goma
in the build configuration but it will take even longer (hours even on a beefy machine).

You will almost certainly need to update the Chromium exploit in healthcheck (`healthcheck/kernel_exploit.html`) if you rebuild Chromium.
There are instructions on how to do that near the top of the exploit.

If you get errors about importing python module `google.protobuf`, it's because you've installed some other package that uses the Google
namespace (I think some of the gcp sdk does this) and made `google.protobuf` inaccessible (see [here](https://github.com/protocolbuffers/protobuf/issues/1296)).
The solution for me was to uninstall all google-related packages from pip.


Source: https://github.com/google/google-ctf/tree/main/2021/quals/pwn-fullchain



(venv) D:\git eno\bazaar-of-ctfd-challenges\github-google-ctf\2021\quals-pwn-fullchain>docker build -t registry.gitlab.com/jointcyberrange.nl/bazaar-of-ctfd-challenges/google-2021-quals-pwn-fullchain .   
[+] Building 2.5s (14/18)
 => [internal] load .dockerignore                                                                                                                                                                                           0.1s 
 => => transferring context: 116B                                                                                                                                                                                           0.0s 
 => [internal] load build definition from Dockerfile                                                                                                                                                                        0.2s 
 => => transferring dockerfile: 1.76kB                                                                                                                                                                                      0.0s 
 => [internal] load metadata for gcr.io/kctf-docker/challenge@sha256:56f7dddff69d08d4d19f4921c724d438cf4d59e434c601f9776fd818368b7107                                                                                       0.5s 
 => [internal] load metadata for docker.io/library/ubuntu:20.04                                                                                                                                                             1.5s 
 => [internal] load build context                                                                                                                                                                                           0.2s 
 => => transferring context: 3.19kB                                                                                                                                                                                         0.0s 
 => [stage-2 1/4] FROM gcr.io/kctf-docker/challenge@sha256:56f7dddff69d08d4d19f4921c724d438cf4d59e434c601f9776fd818368b7107                                                                                                 0.0s 
 => CACHED [attachments 1/4] FROM docker.io/library/ubuntu:20.04@sha256:80ef4a44043dec4490506e6cc4289eeda2d106a70148b74b5ae91ee670e9c35d                                                                                    0.0s 
 => CANCELED [chroot 2/6] RUN /usr/sbin/useradd --no-create-home -u 1000 user                                                                                                                                               0.8s 
 => CANCELED [attachments 2/4] RUN apt update && apt install -yq wget unzip                                                                                                                                                 0.7s 
 => CACHED [chroot 3/6] RUN apt update && apt install -yq qemu-system-x86 python3                                                                                                                                           0.0s 
 => CACHED [attachments 3/4] RUN cd /tmp && wget "https://storage.googleapis.com/gctf-2021-attachments-project/c12856fc6c010d643763e678265f7921b7a44dcd7bcb5ced32634d21dfdff0c5f9542d6a5bdcc6639d8834ab1ff25b263affd8952b1  0.0s 
 => CACHED [attachments 4/4] RUN cd /tmp && unzip c12856fc6c010d643763e678265f7921b7a44dcd7bcb5ced32634d21dfdff0c5f9542d6a5bdcc6639d8834ab1ff25b263affd8952b11e972c2066aa3cae71540 rootfs.img                               0.0s 
 => CACHED [chroot 4/6] COPY --from=attachments /tmp/rootfs.img /home/user/                                                                                                                                                 0.0s 
 => ERROR [chroot 5/6] COPY bzImage run_qemu.py flag /home/user/                                                                                                                                                            0.0s 
------
 > [chroot 5/6] COPY bzImage run_qemu.py flag /home/user/:
------
Dockerfile:32
--------------------
  30 |     COPY --from=attachments /tmp/rootfs.img /home/user/
  31 |
  32 | >>> COPY bzImage run_qemu.py flag /home/user/
  33 |     RUN chmod 755 /home/user/run_qemu.py && chmod 644 /home/user/rootfs.img
  34 |
--------------------
ERROR: failed to solve: failed to compute cache key: failed to calculate checksum of ref 2146e489-f001-4642-9cd0-e47099b7d5f7::ox1mqrinbfascztd2llvbtc53: "/bzImage": not found