# Source: DOCKER-BUILD-FAILS

# TriDroid

Author: sajjadium@

TL;DR: Vulnerable Android app. The flag is kept in a textbox and can be logged by calling a specific Java method. Players have to first exploit the JavaScript vulnerability and then exploit the underlying native library to get a RCE which can be used to call the logging method.


Source: https://github.com/google/google-ctf/tree/main/2021/quals/pwn-tridroid
Note: Error while docker build.


(venv) D:\git eno\bazaar-of-ctfd-challenges\github-google-ctf\2021\quals-pwn-tridroid>docker build -t registry.gitlab.com/jointcyberrange.nl/bazaar-of-ctfd-challenges/google-2021-quals-pwn-tridroid .
[+] Building 1.4s (12/20)
 => [internal] load build definition from Dockerfile                                                                                                                                                                        0.1s 
 => => transferring dockerfile: 2.47kB                                                                                                                                                                                      0.0s 
 => [internal] load .dockerignore                                                                                                                                                                                           0.0s 
 => => transferring context: 2B                                                                                                                                                                                             0.0s 
 => [internal] load metadata for gcr.io/kctf-docker/challenge@sha256:56f7dddff69d08d4d19f4921c724d438cf4d59e434c601f9776fd818368b7107                                                                                       0.0s 
 => [internal] load metadata for docker.io/library/ubuntu:20.04                                                                                                                                                             0.6s 
 => [chroot  1/11] FROM docker.io/library/ubuntu:20.04@sha256:80ef4a44043dec4490506e6cc4289eeda2d106a70148b74b5ae91ee670e9c35d                                                                                              0.0s 
 => [internal] load build context                                                                                                                                                                                           0.1s 
 => => transferring context: 113B                                                                                                                                                                                           0.0s 
 => [stage-1 1/4] FROM gcr.io/kctf-docker/challenge@sha256:56f7dddff69d08d4d19f4921c724d438cf4d59e434c601f9776fd818368b7107                                                                                                 0.0s 
 => CACHED [chroot  2/11] RUN /usr/sbin/useradd --no-create-home -u 1000 user                                                                                                                                               0.0s 
 => CACHED [chroot  3/11] RUN set -e -x;         apt update -y;         apt upgrade -y;  apt install -y software-properties-common;  apt install -y openjdk-11-jdk;  apt install -y unzip wget less;  apt install -y cpu-c  0.0s 
 => CACHED [chroot  4/11] RUN set -e -x;  wget https://dl.google.com/android/repository/commandlinetools-linux-6514223_latest.zip -O commandlinetools.zip;  mkdir -p /opt/android/sdk/cmdline-tools;  unzip commandlinetoo  0.0s 
 => CACHED [chroot  5/11] RUN set -e -x;  yes | sdkmanager --install   "cmdline-tools;latest"   "platform-tools"   "build-tools;30.0.0"   "platforms;android-30"   "system-images;android-30;google_apis;x86_64"   "emulat  0.0s 
 => ERROR [chroot  6/11] RUN sdkmanager --update;                                                                                                                                                                           0.7s 
------
 > [chroot  6/11] RUN sdkmanager --update;:
#0 0.625 Error: LinkageError occurred while loading main class com.android.sdklib.tool.sdkmanager.SdkManagerCli
#0 0.625        java.lang.UnsupportedClassVersionError: com/android/sdklib/tool/sdkmanager/SdkManagerCli has been compiled by a more recent version of the Java Runtime (class file version 61.0), this version of the Java Runtime only recognizes class file versions up to 55.0
------
Dockerfile:48
--------------------
  46 |                  "emulator";
  47 |
  48 | >>> RUN sdkmanager --update;
  49 |
  50 |     ENV PATH "/opt/android/sdk/emulator:${PATH}"
--------------------
ERROR: failed to solve: process "/bin/sh -c sdkmanager --update;" did not complete successfully: exit code: 1