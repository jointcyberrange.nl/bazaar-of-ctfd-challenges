# Status: DOCKER-BUILD-FAILS

# Memsafety

Challenge will exploit https://github.com/rust-lang/rust/issues/80895 -- a proof-of-concept is available at https://godbolt.org/z/MzWjfnvoc.

The scenario such that users can provide a snippet of Rust code which will be compiled into a program vulnerable to the above issue. We'll write a simple parser to check/constrain the code users provide, although defeating the sandbox to get RCE will also be considered a solution (although not the intended one). The flag will reside in memory, with the intended solution being that the heap overflow is used to search memory for the flag.


Source: https://github.com/google/google-ctf/tree/main/2021/quals/pwn-memsafety
Note: Error while dockerbuild.

(venv) D:\git eno\bazaar-of-ctfd-challenges\github-google-ctf\2021\quals-pwn-memsafety>docker build -t registry.gitlab.com/jointcyberrange.nl/bazaar-of-ctfd-challenges/google-2021-quals-pwn-memsafety .
[+] Building 2.8s (10/24)
 => [internal] load .dockerignore                                                                                                                                                                                           0.0s 
 => => transferring context: 2B                                                                                                                                                                                             0.0s 
 => [internal] load build definition from Dockerfile                                                                                                                                                                        0.1s 
 => => transferring dockerfile: 2.35kB                                                                                                                                                                                      0.0s 
 => [internal] load metadata for gcr.io/kctf-docker/challenge@sha256:56f7dddff69d08d4d19f4921c724d438cf4d59e434c601f9776fd818368b7107                                                                                       0.0s 
 => [internal] load metadata for docker.io/library/ubuntu:20.10                                                                                                                                                             0.8s 
 => [chroot  1/15] FROM docker.io/library/ubuntu:20.10@sha256:a7b08558af07bcccca994b01e1c84f1d14a2156e0099fcf7fcf73f52d082791e                                                                                              0.0s 
 => [internal] load build context                                                                                                                                                                                           0.1s 
 => => transferring context: 1.33kB                                                                                                                                                                                         0.0s 
 => [stage-1 1/4] FROM gcr.io/kctf-docker/challenge@sha256:56f7dddff69d08d4d19f4921c724d438cf4d59e434c601f9776fd818368b7107                                                                                                 0.0s 
 => CACHED [chroot  2/15] RUN /usr/sbin/useradd --no-create-home -u 1000 user                                                                                                                                               0.0s 
 => CACHED [chroot  3/15] RUN ln -s `which python3` /usr/bin/python                                                                                                                                                         0.0s 
 => ERROR [chroot  4/15] RUN set -ex; apt-get update -y; apt-get upgrade -y; apt-get install -y rustc=1.47.0+dfsg1+llvm-1ubuntu1~20.10; apt-get install -y python3; apt-get install -y build-essential; apt-get install -y  1.8s 
------
 > [chroot  4/15] RUN set -ex; apt-get update -y; apt-get upgrade -y; apt-get install -y rustc=1.47.0+dfsg1+llvm-1ubuntu1~20.10; apt-get install -y python3; apt-get install -y build-essential; apt-get install -y cargo; apt-get install -y curl:
#0 0.370 + apt-get update -y
#0 0.519 Ign:1 http://security.ubuntu.com/ubuntu groovy-security InRelease
#0 0.644 Ign:2 http://archive.ubuntu.com/ubuntu groovy InRelease
#0 0.759 Err:3 http://security.ubuntu.com/ubuntu groovy-security Release
#0 0.759   404  Not Found [IP: 185.125.190.36 80]
#0 0.869 Ign:4 http://archive.ubuntu.com/ubuntu groovy-updates InRelease
#0 1.098 Ign:5 http://archive.ubuntu.com/ubuntu groovy-backports InRelease
#0 1.329 Err:6 http://archive.ubuntu.com/ubuntu groovy Release
#0 1.329   404  Not Found [IP: 91.189.91.83 80]
#0 1.593 Err:7 http://archive.ubuntu.com/ubuntu groovy-updates Release
#0 1.593   404  Not Found [IP: 91.189.91.83 80]
#0 1.670 Err:8 http://archive.ubuntu.com/ubuntu groovy-backports Release
#0 1.670   404  Not Found [IP: 91.189.91.83 80]
#0 1.678 Reading package lists...
#0 1.691 E: The repository 'http://security.ubuntu.com/ubuntu groovy-security Release' does not have a Release file.
#0 1.691 E: The repository 'http://archive.ubuntu.com/ubuntu groovy Release' does not have a Release file.
#0 1.691 E: The repository 'http://archive.ubuntu.com/ubuntu groovy-updates Release' does not have a Release file.
#0 1.691 E: The repository 'http://archive.ubuntu.com/ubuntu groovy-backports Release' does not have a Release file.
------
Dockerfile:20
--------------------
  18 |     RUN ln -s `which python3` /usr/bin/python
  19 |
  20 | >>> RUN set -ex; apt-get update -y; apt-get upgrade -y; apt-get install -y rustc=1.47.0+dfsg1+llvm-1ubuntu1~20.10; apt-get install -y python3; apt-get install -y build-essential; apt-get install -y cargo; apt-get install -y curl
  21 |
  22 |     RUN set -ex; apt-get install -y strace
--------------------
ERROR: failed to solve: process "/bin/sh -c set -ex; apt-get update -y; apt-get upgrade -y; apt-get install -y rustc=1.47.0+dfsg1+llvm-1ubuntu1~20.10; apt-get install -y python3; apt-get install -y build-essential; apt-get install -y cargo; apt-get install -y curl" did not complete successfully: exit code: 100