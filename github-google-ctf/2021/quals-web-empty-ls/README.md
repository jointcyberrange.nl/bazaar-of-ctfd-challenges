# Status: MISSING-FILES DOCKER-BUILD-FAILS

# License of this file

Copyright 2021 Google LLC

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Author: Kegan Thorrez

# empty ls central

This is the central challenge for empty ls. It should be deployed and visible.

## Flag

The flag is configured in these configs, but the flag file itself is in
empty-ls-admin, since that's the deployment that actually has the flag.

## Service account access

I don't understand how allowing `127.0.0.0/8` allows the code to talk to
`169.254.169.254`, but it does.

### IAM

Give the service account the reCAPTCHA Enterprise > reCAPTCHA Enterprise Agent
role on the project.

Give the service account the DNS > DNS Administrator role on the project.

Those are both done with

```
gcloud projects add-iam-policy-binding \
    --role roles/recaptchaenterprise.agent your-project \
    --member "serviceAccount:empty-ls@your-project.iam.gserviceaccount.com"
gcloud projects add-iam-policy-binding your-project \
    --role roles/dns.admin \
    --member "serviceAccount:empty-ls@your-project.iam.gserviceaccount.com"
```

We better hope the challenge has no RCE or SSRF vuln. The reCAPTCHA Enterprise
role is pretty benign, attackers can't use it for much. But the DNS
Administrator role is pretty powerful, attackers can take down our DNS.

Give the kubernetes service account the ability to become this challenge's
service account:

```
gcloud iam service-accounts add-iam-policy-binding \
    --role roles/iam.workloadIdentityUser \
    --member "serviceAccount:your-project.svc.id.goog[default/empty-ls]"
    empty-ls@your-project.iam.gserviceaccount.com
```

Yes those are literal brackets. See

https://cloud.google.com/kubernetes-engine/docs/how-to/workload-identity#authenticating\_to

Apparently this doesn't need to get checked in to piper.

## Recaptcha

This uses reCAPTCHA Enterprise. Make sure the reCAPTCHA Enterprise settings
allow whatever domain you're using.

Also make sure the static html pages as well as central.go have the correct key
id for the reCAPTCHA Enterprise key.

## GCP Project

central.go needs to specify the correct GCP project, both to get reCAPTCHA
Enterprise working as well as to get Cloud DNS working.

Source: https://github.com/google/google-ctf/tree/main/2021/quals/web-empty-ls


ERROR CODE:

(venv) D:\git eno\bazaar-of-ctfd-challenges-1\github-google-ctf\2021\quals-web-empty-ls>docker build -t registry.gitlab.com/jointcyberrange.nl/bazaar-of-ctfd-challenges-1/quals-web-empty-ls .
[+] Building 0.5s (15/16)
 => [internal] load .dockerignore                                                                                                                                                                                           0.0s 
 => => transferring context: 2B                                                                                                                                                                                             0.0s 
 => [internal] load build definition from Dockerfile                                                                                                                                                                        0.0s 
 => => transferring dockerfile: 1.59kB                                                                                                                                                                                      0.0s 
 => [internal] load metadata for gcr.io/kctf-docker/challenge@sha256:56f7dddff69d08d4d19f4921c724d438cf4d59e434c601f9776fd818368b7107                                                                                       0.0s 
 => CACHED [ 1/12] FROM gcr.io/kctf-docker/challenge@sha256:56f7dddff69d08d4d19f4921c724d438cf4d59e434c601f9776fd818368b7107                                                                                                0.0s 
 => [internal] load build context                                                                                                                                                                                           0.0s 
 => => transferring context: 88.18kB                                                                                                                                                                                        0.0s 
 => CANCELED [ 2/12] RUN wget -q https://golang.org/dl/go1.16.5.linux-amd64.tar.gz                                                                                                                                          0.4s 
 => CACHED [ 3/12] RUN rm -rf /usr/local/go && tar -C /usr/local -xzf go1.16.5.linux-amd64.tar.gz                                                                                                                           0.0s 
 => CACHED [ 4/12] COPY go.mod go.sum central.go /home/user/                                                                                                                                                                0.0s 
 => CACHED [ 5/12] COPY captcha /home/user/captcha                                                                                                                                                                          0.0s 
 => CACHED [ 6/12] COPY clientcrtgen /home/user/clientcrtgen                                                                                                                                                                0.0s 
 => CACHED [ 7/12] COPY contacthandler /home/user/contacthandler                                                                                                                                                            0.0s 
 => CACHED [ 8/12] COPY sitehandler /home/user/sitehandler                                                                                                                                                                  0.0s 
 => CACHED [ 9/12] COPY userhandler /home/user/userhandler                                                                                                                                                                  0.0s 
 => CACHED [10/12] RUN cd /home/user && go build central.go                                                                                                                                                                 0.0s 
 => ERROR [11/12] COPY zone443.dev.fullchain.crt.pem      zone443.dev.key.pem      clientca.crt.pem      clientca.key.pem      bad_words.b64.txt      /home/user/                                                           0.0s 
------
 > [11/12] COPY zone443.dev.fullchain.crt.pem      zone443.dev.key.pem      clientca.crt.pem      clientca.key.pem      bad_words.b64.txt      /home/user/:
------
Dockerfile:31
--------------------
  30 |
  31 | >>> COPY zone443.dev.fullchain.crt.pem \
  32 | >>>      zone443.dev.key.pem \
  33 | >>>      clientca.crt.pem \
  34 | >>>      clientca.key.pem \
  35 | >>>      bad_words.b64.txt \
  36 | >>>      /home/user/
  37 |     COPY static /home/user/static
--------------------
ERROR: failed to solve: failed to compute cache key: failed to calculate checksum of ref 2146e489-f001-4642-9cd0-e47099b7d5f7::mgbyzbm3svgb2enw2oczk5erc: "/zone443.dev.key.pem": not found