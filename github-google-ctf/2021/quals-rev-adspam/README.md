# Status: OK

# AdSpam

An dummy ad spam bot for Android. The contestants are expected to reverse engineer the C&C protocol and find a way to break the license. 

The challenge does not need anything pre-built.

Source: https://github.com/google/google-ctf/tree/main/2021/quals/rev-adspam