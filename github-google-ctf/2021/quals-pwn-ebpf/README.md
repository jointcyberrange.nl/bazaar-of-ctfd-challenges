# Status: DOCKER-BUILD-FAILS

# EBPF

The challenge is a minimal Linux system based on the kernel version 5.12.2. A vulnerability was introduced into the eBPF verifier and the players are expected to elevate privileges to root and read the otherwise protected `/flag`.

## Build and Deploy

```
cd pwn-ebpf
make -C challenge && kctf chal start
```

## Healthcheck

Note, that healthcheck has the Makefile as it needs to compile the exploit first.

```
make -C healthcheck
```



Source: https://github.com/google/google-ctf/tree/main/2021/quals/pwn-ebpf
Note: Error while docker build.



(venv) D:\git eno\bazaar-of-ctfd-challenges\github-google-ctf\2021\quals-pwn-ebpf>docker build -t registry.gitlab.com/jointcyberrange.nl/bazaar-of-ctfd-challenges/google-2021-quals-pwn-ebpf .
[+] Building 1.2s (10/16)
 => [internal] load build definition from Dockerfile                                                                                                                                                                        0.1s 
 => => transferring dockerfile: 1.42kB                                                                                                                                                                                      0.0s 
 => [internal] load .dockerignore                                                                                                                                                                                           0.0s 
 => => transferring context: 2B                                                                                                                                                                                             0.0s 
 => [internal] load metadata for gcr.io/kctf-docker/challenge@sha256:56f7dddff69d08d4d19f4921c724d438cf4d59e434c601f9776fd818368b7107                                                                                       0.0s 
 => [internal] load metadata for docker.io/library/ubuntu:20.10                                                                                                                                                             0.6s 
 => [internal] load build context                                                                                                                                                                                           0.0s 
 => => transferring context: 60B                                                                                                                                                                                            0.0s 
 => CACHED [chroot 1/7] FROM docker.io/library/ubuntu:20.10@sha256:a7b08558af07bcccca994b01e1c84f1d14a2156e0099fcf7fcf73f52d082791e                                                                                         0.0s 
 => [stage-1 1/4] FROM gcr.io/kctf-docker/challenge@sha256:56f7dddff69d08d4d19f4921c724d438cf4d59e434c601f9776fd818368b7107                                                                                                 0.0s 
 => CANCELED [chroot 2/7] RUN /usr/sbin/useradd --no-create-home -u 1000 user                                                                                                                                               0.4s 
 => CACHED [chroot 3/7] RUN apt update && apt install -yq qemu-system-x86                                                                                                                                                   0.0s 
 => ERROR [chroot 4/7] COPY bzImage root.cpio.gz start.sh /home/user/                                                                                                                                                       0.0s 
------
 > [chroot 4/7] COPY bzImage root.cpio.gz start.sh /home/user/:
------
Dockerfile:23
--------------------
  21 |     RUN apt update && apt install -yq qemu-system-x86
  22 |
  23 | >>> COPY bzImage root.cpio.gz start.sh /home/user/
  24 |     RUN chmod 755 /home/user/start.sh
  25 |     RUN chmod 644 /home/user/bzImage
--------------------
ERROR: failed to solve: failed to compute cache key: failed to calculate checksum of ref 2146e489-f001-4642-9cd0-e47099b7d5f7::e6yj1k0sv8rppr7v4eli1ppdb: "/root.cpio.gz": not found