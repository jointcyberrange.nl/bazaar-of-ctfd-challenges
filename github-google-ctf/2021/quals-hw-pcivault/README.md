# Status: DOCKER-BUILD-FAILS

# PCIVault challenge

The challenge consists out of two VMs and an emulated PCI device (`emulator`
binary). The contestants will get access to the serial port of one of the VMs
and need to exploit bugs in the PCI device to get code execution there and
finally on the secondary flag-VM which is also connected to the device.


Source: https://github.com/google/google-ctf/tree/main/2021/quals/hw-pcivault

Note: Docker build doesn't work. 
Error code:

 => ERROR [build_files 2/3] RUN apt-get update && DEBIAN_FRONTEND="noninteractive" apt-get install -y wget                                                                                                                  3.0s 
------
 > [build_files 2/3] RUN apt-get update && DEBIAN_FRONTEND="noninteractive" apt-get install -y wget:
#0 1.220 Ign:1 http://security.ubuntu.com/ubuntu groovy-security InRelease
#0 1.284 Ign:2 http://archive.ubuntu.com/ubuntu groovy InRelease
#0 1.509 Err:3 http://security.ubuntu.com/ubuntu groovy-security Release
#0 1.509   404  Not Found [IP: 91.189.91.83 80]
#0 1.601 Ign:4 http://archive.ubuntu.com/ubuntu groovy-updates InRelease
#0 1.971 Ign:5 http://archive.ubuntu.com/ubuntu groovy-backports InRelease
#0 2.234 Err:6 http://archive.ubuntu.com/ubuntu groovy Release
#0 2.234   404  Not Found [IP: 91.189.91.82 80]
#0 2.549 Err:7 http://archive.ubuntu.com/ubuntu groovy-updates Release
#0 2.549   404  Not Found [IP: 91.189.91.82 80]
#0 2.855 Err:8 http://archive.ubuntu.com/ubuntu groovy-backports Release
#0 2.855   404  Not Found [IP: 91.189.91.82 80]
#0 2.935 Reading package lists...
#0 2.950 E: The repository 'http://security.ubuntu.com/ubuntu groovy-security Release' does not have a Release file.
#0 2.950 E: The repository 'http://archive.ubuntu.com/ubuntu groovy Release' does not have a Release file.
#0 2.950 E: The repository 'http://archive.ubuntu.com/ubuntu groovy-updates Release' does not have a Release file.
#0 2.950 E: The repository 'http://archive.ubuntu.com/ubuntu groovy-backports Release' does not have a Release file.
------
Dockerfile:22
--------------------
  20 |     ENV HASH=aa81c10a557f4c591e7c91aaeb2342ced4529f96236ebd2e8e0de277b446d9214370ec924b52a831876904b6e2ff80399a642cb980fc73b68747038d452bae6c
  21 |
  22 | >>> RUN apt-get update && DEBIAN_FRONTEND="noninteractive" apt-get install -y wget
  23 |
  24 |     RUN cd /tmp && wget https://storage.googleapis.com/gctf-2021-attachments-project/${HASH} -O build.tar.gz \
--------------------
ERROR: failed to solve: process "/bin/sh -c apt-get update && DEBIAN_FRONTEND=\"noninteractive\" apt-get install -y wget" did not complete successfully: exit code: 100