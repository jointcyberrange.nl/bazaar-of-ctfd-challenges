# Status: OK

# Diffie-Hellman Key Exchange

# Description
Unzip het bijgeleverde diffie-hellman.zip bestand en bestudeer het stukje code crypto_basic.py

Door een foutje in de implementatie van deze code, door het niet gebruiken van een prime getal, zullen de mogelijke waardes voor SharedSecret maar weinig verschillende getallen zijn, in plaats van boven de 200 verschillende waardes. En daardoor veel eenvoudiger te kraken. 

Welke waardes kan de SharedSecret aannemen door in dit algoritme enkel de variable van bobSecret aan te passen? Geen deze getallen in oplopende volgorde in het volgende format als vlag.

Flag format = JCR(1,2,3,...,...,...)