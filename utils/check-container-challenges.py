# refactoring by Genie.ai
import os
import yaml

def check_challenge_files(directory):
    errors_found = False
# docstring by continue.dev
    """
    Checks the challenge files in the given directory and its subdirectories.

    This function walks through the directory and its subdirectories, looking for 'challenge.yml' or 'challenge.yaml' files.
    If a 'challenge.yml' or 'challenge.yaml' file is found, it checks the following:

    1. If the 'type' field in the challenge data is 'container', it looks for a 'docker-compose.yml' or 'docker-compose.yaml' file in the same directory.
       - If the 'docker-compose.yml' or 'docker-compose.yaml' file is not found, it prints an error message.
       - If the file is found, it compares the content of the file with the 'extra: compose' field in the challenge data.
         If they differ, it prints an error message.

    If no errors are found, it prints "No errors found."
    If any errors are encountered, it raises an Exception with the message "Some container type challenges are missing a docker-compose.yml file".

    Args:
        directory (str): The path to the directory to be checked.

    Raises:
        Exception: If any errors are encountered during the check.
    """
    errors_found = False
    
    for root, dirs, files in os.walk(directory):
        if "challenge.yml" in files:
            challenge_path = os.path.join(root, "challenge.yml")
        elif "challenge.yaml" in files:
            challenge_path = os.path.join(root, "challenge.yaml")
        else:
            continue
        
        with open(challenge_path, 'r') as f:
            challenge_data = yaml.safe_load(f)
            
            if 'type' in challenge_data and challenge_data['type'] == 'container':
                compose_file = "docker-compose.yml" if "docker-compose.yml" in files else "docker-compose.yaml"
                compose_path = os.path.join(root, compose_file)
                
                if not os.path.exists(compose_path):
                    print(f"Error: 'container' type in {challenge_path}, but '{compose_file}' does not exist in the same directory.")
                    errors_found = True
                else:
                    with open(compose_path, 'r') as compose_file_handle:
                        compose_content = compose_file_handle.read()
                        if 'extra' in challenge_data and 'compose' in challenge_data['extra']:
                            if yaml.safe_load(challenge_data['extra']['compose']) != yaml.safe_load(compose_content):
                                print(f"Error: Content of '{compose_file}' differs from the 'extra: compose' field in {challenge_path}.")
                                errors_found = True

    if not errors_found:
        print("No errors found.")
    else:
        raise Exception("Some container type challenges are missing a docker-compose.yml file")

# Example usage:
# directory_path = '.'  # Current directory
# check_challenge_files(directory_path)
