import yaml

def extract_and_write_compose_section(input_file, output_file):
    # Open the input file and load its contents as a YAML object
    with open(input_file, 'r') as f:
        data = yaml.safe_load(f)

    # Extract the 'compose:' section from the loaded data
    compose_section = data.get('extra', {}).get('compose')

    if compose_section is None:
        print("No 'compose:' section found in the input file.")
        return

    # Write the 'compose:' section to the output file
    with open(output_file, 'w') as f:
        f.write(compose_section)

# Call the function to extract and write the 'compose:' section
extract_and_write_compose_section('challenge.yml', 'docker-compose.yml')
