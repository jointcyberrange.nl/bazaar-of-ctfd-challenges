#!/bin/bash

# Loop through subdirectories
for dir in */; do
    # Save the current directory
    current_dir=$(pwd)
    cd "$dir" || exit

    # Read data from metadata.yml
    version="0.1"
    # Extracting name and removing any extra text
    name=$(awk '/^name:/ {gsub("\"", ""); print substr($0, index($0, $2))}' metadata.yml | awk -F: '{print $1}')
    author=$(awk '/author:/ {print $2}' metadata.yml)
    category=$(awk '/category:/ {print $2}' metadata.yml)
    description=$(awk '/description:/ {gsub(/^description: */, ""); sub(/^[[:space:]]*/, "", $0); gsub(/[[:space:]]+/, " ", $0); printf "\"%s", $0; while(getline){if($1=="value:"){break}; gsub(/[[:space:]]+/, " ", $0); printf " %s", $0}}' metadata.yml)
    description="${description}\""
    value=$(awk '/^value:/ {print $2}' metadata.yml)
    flag=$(awk '/flag:/ {print $2}' metadata.yml)
    topics=$(awk '/topics:/ {getline; print}' metadata.yml)
    tags=$(awk '/tags:/ {getline; print}' metadata.yml)
    files=$(awk '/files:/ {getline; print}' metadata.yml)
    state=$(awk '/state:/ {print $2}' metadata.yml)
    image=$(awk '/image:/ {print $2}' metadata.yml)
    host=$(awk '/host:/ {print $2}' metadata.yml)
    port=$(awk '/port:/ {print $2}' metadata.yml)

    # If any section is not specified, set it to an empty string
    if [ -z "$category" ]; then
        category=""
    fi

    if [ -z "$topics" ]; then
        topics=""
    fi

    if [ -z "$tags" ]; then
        tags=""
    fi

    if [ -z "$files" ]; then
        files=""
    fi

    # Set the state to "visible"
    state="visible"

    # Set attempts to 0
    attempts=0

    if [ -z "$image" ]; then
        image=""
    fi

    if [ -z "$host" ]; then
        host=""
    fi

    # Check if Dockerfile exists in the current directory
    if [ -n "$(find . -name Dockerfile -print -quit)" ]; then
        extra="extra:
        initial: 500
        decay: 100
        minimum: 50

        challenge_type: web
        compose: | 
            version: '3.3'

            services:
              bsides-sf-2020-$name:
                container_name: bsides-sf-2020-$name
                image: $image
                ports:
                  - $port
                labels:
                  kompose.service.type: nodeport"
    fi

    # Check if Dockerfile exists in the current directory
    if [ -n "$(find . -name Dockerfile -print -quit)" ]; then
        type="container"
        # Create docker-compose.yml
        cat <<EOF > docker-compose.yml
version: '3.3'

services:
   bsides-sf-2020-$name:
     container_name: bsides-sf-2020-$name
     image: $image
     ports:
       - $port
     labels:
       kompose.service.type: nodeport
EOF

        echo "docker-compose.yml file created."
    else
        type="standard"
    fi

    # Create challenge.yml with the read data
    cat <<EOF > challenge.yml
# This file represents the base specification of your challenge. It is used by
# other tools to install and deploy your challenge.

# Required sections
name: "$name"
author: "$author"
category: "$category"
description: $description
value: $value
type: $type

# The extra field provides additional fields for data during the install/sync commands/
# Fields in extra can be used to supply additional information for other challenge types
# For example the follow extra field is for dynamic challenges. To use these following
# extra fields, set the type to "dynamic" and uncomment the "extra" section below
$(if [ "$type" = "container" ]; then
echo "$extra"
fi)

# Settings used for Dockerfile deployment
# If not used, remove or set to null
# If you have a Dockerfile set to .
# If you have an imaged hosted on Docker set to the image url (e.g. python/3.8:latest, registry.gitlab.com/python/3.8:latest)
# Follow Docker best practices and assign a tag
image: $image
# Specify a host to deploy the challenge onto.
# The currently supported URI schemes are ssh:// and registry://
# ssh is an ssh URI where the above image will be copied to and deployed (e.g. ssh://root@123.123.123.123)
# registry is a Docker registry tag (e.g registry://registry.example.com/test/image)
# host can also be specified during the deploy process: `ctf challenge deploy challenge --host=ssh://root@123.123.123.123`
host: $host

# Can be removed if unused
attempts: $attempts

# Flags specify answers that your challenge use. You should generally provide at least one.
# Can be removed if unused
# Accepts strings or dictionaries of CTFd API data
flags:
    - $flag

# Topics are used to help tell what techniques/information a challenge involves
# They are generally only visible to admins
# Accepts strings
topics:
$(awk '/topics:/ {print "    - "$2}' metadata.yml | sed 's/- //')

# Tags are used to provide additional public tagging to a challenge
# Can be removed if unused
# Accepts strings
tags:
$(echo "$tags")

# Provide paths to files from the same directory that this file is in
# Accepts strings
files:
$(echo "$files")

# The state of the challenge.
# If the field is omitted, the challenge is visible by default.
# If provided, the field can take one of two values: hidden, visible.
state: $state

# Specifies what version of the challenge specification was used.
# Subject to change until ctfcli v1.0.0
version: "$version"

EOF

    echo "Challenge YAML file created."

    # Return to the original directory
    cd "$current_dir" || exit
done
