#!/bin/bash

# Function to check readme files
check_readme_files() {
    local readme_files=($(find "$1" -type f -iname "readme.md" -not -path "./Readme.md"))
    declare -A status_files_map=()
    
    # Populate status_files_map
    for file in "${readme_files[@]}"; do
        statuses=$(grep -iPo '#\s*Status:\s*\K[\w\- ]+' "$file")
        if [ -z "$statuses" ]; then
            statuses="STATUS-MISSING"
        fi
        IFS=' ' read -ra status_array <<< "$statuses"
        for status in "${status_array[@]}"; do
            if [ "$status" != "OK" ]; then
                status_files_map["$status"]+=$(realpath --relative-to="$1" "$file")$'\n'
            fi
        done
    done

    # Print results
    for status in "${!status_files_map[@]}"; do
        echo
        echo "______________"
        echo "Status: $status"
        echo "${status_files_map[$status]}" | sort
        echo "______________"
        echo
    done
}

# Start checking from the current directory
check_readme_files .
