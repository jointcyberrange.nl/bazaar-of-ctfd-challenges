import os
import yaml

def check_challenge_yml(file_path):
    errorHappened = False
    with open(file_path, 'r') as file:
        data = yaml.safe_load(file)

        if not isinstance(data.get('name'), str):
            print(f"Error: 'name' field in {file_path} is not a string.")
            errorHappened = True
        if not isinstance(data.get('author'), str):
            print(f"Error: 'author' field in {file_path} is not a string.")
            errorHappened = True
        if not isinstance(data.get('description'), str):
            print(f"Error: 'description' field in {file_path} is not a string.")
            errorHappened = True
        if not isinstance(data.get('category'), str):
            print(f"Error: 'category' field in {file_path} is not a string.")
            errorHappened = True
        if data.get('version') != "0.1":
            print(f"Error: 'version' field in {file_path} is not '0.1'.")
            errorHappened = True
        if data.get('type') != 'standard' and data.get('type') != 'container':
            print(f"Error: 'type' field in {file_path} is not 'standard' or 'container'.")
            errorHappened = True
        flags = data.get('flags')
        if not flags or not isinstance(flags, list):
            print(f"Error: 'flags' field in {file_path} must contain at least one valid flag.")
            errorHappened = True
        else:
            for flag in flags:
                if isinstance(flag, str):
                    if len(flag) == 0:
                        print(f"Error: Flag may not be an empty string in {file_path}.")
                        errorHappened = True
                    else:
                        continue
                elif isinstance(flag, dict):
                    if 'content' not in flag or not isinstance(flag['content'], str):
                        print(f"Error: Invalid flag format in {file_path}. 'content' field must be a string.")
                        errorHappened = True
                    elif 'type' not in flag or flag['type'] not in ["static", "regex"]:
                        print(f"Error: Invalid flag type in {file_path}. 'type' must be 'static' or 'regex'.")
                        errorHappened = True
                    elif 'data' in flag and flag['data'] not in ["case_sensitive", "case_insensitive"]:
                        print(f"Error: Invalid flag data in {file_path}. 'data' must be 'case_sensitive' or 'case_insensitive'.")
                        errorHappened = True
                else:
                    print(f"Error: Invalid flag format in {file_path}.")
                    errorHappened = True
    return errorHappened

def iterate_files(directory):
    errorHappened = False
    for root, dirs, files in os.walk(directory):
        for file in files:
            if file.endswith(".yml") or file.endswith(".yaml"):
                if file == 'challenge.yml':
                    errorHappened = check_challenge_yml(os.path.join(root, file)) or errorHappened
    if errorHappened:
        raise Exception("Some challenge files contain errors")
    else:
        print("No errors found")

# Change the directory path to the desired directory
directory_path = '.'  # Current directory
iterate_files(directory_path)
