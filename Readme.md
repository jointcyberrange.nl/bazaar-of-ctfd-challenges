# Challenge collection for CTFd

We are on a mission #1KC to collect 1000 challenges that can be automatically deployed to CTFd <https://ctfd.io>!

Each challenge is a directory with a ```challenge.yml``` file. Once committed and pushed to this repository, the challenge will be automatically checked against certain criteria. These challenges then can be deployed automatically to a CTFd instance.

The automatic deployment is over the CTFd API, as implemented in `ctfcli`.

A more extensive description of workflows is in <https://gitlab.com/jointcyberrange.nl/ctfcli-testchallenges>.

This is now work in progress. Feel free to contribute, raise issues, and give tips.

[![pipeline status](https://gitlab.com/jointcyberrange.nl/bazaar-of-ctfd-challenges/badges/main/pipeline.svg)](https://gitlab.com/jointcyberrange.nl/bazaar-of-ctfd-challenges/-/commits/main)

## Rights

We are copying and adapting public challenges, which means that we and you should be respecting the relevant rights. Don't hesitate to raise an issue if need be.

## Contributions

We welcome contributions. Either by raising an issue with a tip for a repo, or complete merge requests for sets of challenges.

Two simple examples can be found in arcade/caesar_cipher and arcade/1_html_code.

## Workflow initial setup of ctf with a CTFd instance

This assumes that you have a CTFd instance running on localhost:8000. The suggested approach is to run a Docker image as described in <https://gitlab.com/jointcyberrange.nl/ctfd-docker-with-plugins>.

Windows version. For Mac you need a slightly different venv activate command. In VSCode: use Command Palette

1. `python -m venv venv`
2. `.\venv\Scripts\activate`
3. `pip install ctfcli`
4. `ctf init`
5. CTFd instance URL: `http://localhost:8000`
6. CTFd Admin Access Token: `[ACCESS TOKEN]`
7. Confirm with: `y`
8. `docker login registry.gitlab.com`

## Workflow for each new challenge

Skip steps 1 and 2 if the challenge does not need Docker

1. `docker build -t registry.gitlab.com/jointcyberrange.nl/bazaar-of-ctfd-challenges/[CHALLENGE NAME] .`
2. `docker push registry.gitlab.com/jointcyberrange.nl/bazaar-of-ctfd-challenges/[CHALLENGE NAME]`
3. `ctf challenge add [FOLDER_NAME]`
4. `ctf challenge install [FOLDER_NAME]`

Note: on Mac ARM Silicon you want to use multiarchitecture buildx. The JCR tenants typically run x86.

## Workflow for testing the installation of a subset of challenges

```bash
ls -1 -d */ | xargs -L 1 ctf challenge install
```

Adapt the pattern for `ls` to your liking. Note that `ctf` accepts pathnames as challenge designators, but does not like to work in a subdirectory.

As an example, for testing the `cyberintro` folder, make sure your toplevel `.ctf/config` matches your deployment URL and accesstoken, and use the following (in the devcontainer).

```bash
ls -1 -d cyberintro/*/*/ | xargs -L 1 ctf challenge install
```

## Quality controls for contributions

We have implemented various quality controls for all challenges, which will be applied as a pre-merge check.
There are a bunch of scripts that are run on every pull requests that check the validity of the commited changes and log some useful information:

- YAML syntax is valid and all required fields are included
- The extra.container field matches the contents of a docker-compose file in the same directory (only applicable for container challenges)
- The status of all challenges are logged. Note that this does not fail the pipeline in any way. A missing status will simply result in the challenge being listed under 'STATUS-MISSING'.
- Some statistics on the number of challenges are logged
- Some statistics on the number of container challenges are logged

## Challenge-status

Each challenge can have one of the following statuses:

| Status               | Description                                                                                        |
|----------------------|----------------------------------------------------------------------------------------------------|
| OK                   | Everything works                                                                                   |
| FLAG-MISSING         | The challenge does not have a flag or uses the default flag                                        |
| DESCRIPTION-MISSING  | The challenge does not have a description or uses the default description                          |
| MISSING-FILES        | Some required files are missing, these could be files for the Docker image or the challenge itself |
| DOCKER-BUILD-FAILS   | Docker cannot build the image with the current Dockerfile                                          |
| DOCKERFILE-MISSING   | A Dockerfile is required, but it is missing or empty                                               |
| DOCKER-COMPOSE-FAILS | Docker was able to build the image, but cannot create or start a container with this image         |
| EXTERNAL-DEPENDENCY  | An issue with an external dependency is preventing this challenge from working                     |
| NOT-WORKING          | None of the above scenario's apply, but the challenge is not working yet for an unknown reason     |
| STATUS-MISSING       | The challenge does not have a status listed in its readme.                                         |

New statuses for new problems can be added at any point.

## Source-Repositories

| Repository            | Link                                                                  | Visibility | Directory                                | Status         |
|-----------------------|-----------------------------------------------------------------------|------------|------------------------------------------|----------------|
| Google                | [GitHub](https://github.com/google/google-ctf)                        | Public     | [/github-google-ctf](/github-google-ctf) | Partially Done |
| Challenge The Cyber   | [GitLab](https://gitlab.com/jointcyberrange.nl/challenges-2023/)      | Private    | [/ctc2023](/ctc2023)                     | Done           |
| JCR Arcade            | [GitLab](https://gitlab.com/jointcyberrange.nl/jcr-arcade-challenges) | Private    | [/arcade](/arcade)                       | Done           |
| BSides San Francisco  | [GitHub](https://github.com/BSidesSF)                                 | Public     | [/bsides-sf](/bsides-sf)                 | Partially Done |
| PentestHub            | [GitLab](https://gitlab.com/pentesthub_public/)                       | Public     |                                          | Not Started    |
| TeamItaly             | [GitHub](https://github.com/TeamItaly/TeamItalyCTF-2022)              | Public     |                                          | Not Started    |
| JCR Learningpath Demo | N/A                                                                   | N/A        | [/learningpath](/learningpath)           | Done           |
| 24hiut-2023-cyber     | [GitHub](https://github.com/pandatix/24hiut-2023-cyber) | N/A | N/A | Not Started    |
