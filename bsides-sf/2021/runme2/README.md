# Status: OK

This is a super simple/straight forward level.

Basically, the user sends some code to the server, and the server runs it.
That's it!

Special condition: no null bytes

Source: https://github.com/BSidesSF/ctf-2021-release/tree/main/runme2