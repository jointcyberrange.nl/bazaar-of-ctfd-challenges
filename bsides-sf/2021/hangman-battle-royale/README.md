# Status: DOCKER-BUILD-FAILS

# Challenge

This is an RNG prediction vulnerability. If you play at least 10 rounds, the
names of your opponents are sufficient to get the RNG state!

# Build / deploy

`bundle install && ruby ./hangman`

Or just use Docker. :)

Source: https://github.com/BSidesSF/ctf-2021-release/tree/main/hangman-battle-royale